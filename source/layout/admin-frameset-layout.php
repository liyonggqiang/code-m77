<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?=$this->headMeta()?>
<?=$this->headTitle()?>
<?=$this->headLink()?>
<?=$this->headScript()?>
<!--<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>-->
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.9.2.custom.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/admin-style.css"/>
<link rel="stylesheet" type="text/css" href="/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/custom.css"/>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/bootstrap.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/jquery.slimscroll.min.js"></script>
</head>
    <body>
    <?=$this->layout()->content?>
    </body>
    <?=JsUtils::ob_flush()?>
</html>
