<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?=$this->headMeta()?>
<?=$this->headTitle()?>
<?=$this->headLink()?>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/jquery-ui-1.9.2.custom.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/custom.css"/>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/js/jquery.bootbox.js"></script>
<script type="text/javascript" src="/js/lyq.Utils.js"></script>
<script type="text/javascript" src="/js/lyq.RelateFields.js"></script>
<script type="text/javascript" src="/js/lyq.ContextMenu.js"></script>
<script type="text/javascript" src="/js/lyq.ListEx.js"></script>
<script type="text/javascript" src="/js/lyq.DialogEx.js"></script>
<script type="text/javascript" src="/js/lyq.SlaveTable.js"></script>
<script type="text/javascript" src="/js/lyq.AjaxUtils.js"></script>
<script type="text/javascript" src="/js/lyq.TabEx.js"></script>
<?=$this->headScript()?>
<script type="text/javascript">
$.ajaxSetup({
    global: false,
    type: "POST",
    dataType: 'json'
});
</script>
</head>
<body class="iframe_body iframe-mainbar">
<?=$this->layout()->content?>
<?=JsUtils::ob_flush()?>
</body>
</html>
