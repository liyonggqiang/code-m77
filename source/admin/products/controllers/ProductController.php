<?php
/**
 * Description of ProductController
 *
 * @author LYQ <coderfly@163.com>
 */
class Products_ProductController extends BaseController
{
    public function listAction ()
    {
        $this->render('product-list');
    }

    public function ajaxaddAction ()
    {
        
    }
    
    public function popuplistAction ()
    {
        $this->render('popup-product-list');
    }
}
