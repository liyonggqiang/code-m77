<?php

/**
 * Description of ItemController
 *
 * @author LYQ <coderfly@163.com>
 */
class Products_ItemController extends BaseController
{
    public function listAction ()
    {
        $this->render('item-list');
    }

    public function newAction ()
    {
        $this->render('item-info');
    }

    public function addAction ()
    {
        
    }
	
	public function editAction ()
	{
		$this->render('item-info');
	}

    public function ajaxdeleteAction ()
    {
        
    }
	
	public function popuplistAction ()
    {
        $this->render('popup-item-list');
    }
}
