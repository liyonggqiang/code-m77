<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">产品列表</a>
        <ul class="nav nav-pills">
            <li><a href="<?=$this->buildUrl('new')?>">增加</a>
            <li><a href="<?=$this->buildUrl('ajaxdelete')?>" id="btn_nav_delete">删除</a></li>
        </ul>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">产品名称</th>
                        <th width="200">规格</th>
                        <th width="100">售价</th>
                        <th width="200">所属商品系列</th>
                        <th width="50">套装?</th>
                        <th width="100">产地</th>
                        <th>备注</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr link="<?=$this->buildUrl('edit',null,null,array('id'=>1))?>">
                            <td><input type="checkbox" d='1'></td>
                            <td>契尔氏白泥面膜瓶装</td>
                            <td>50ml 瓶装</td>
                            <td>¥205</td>
                            <td>契尔氏白泥面膜</td>
                            <td>N</td>
                            <td>美国</td>
                            <td>-</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				window.location = $(this).attr('link');
            }
        }
    });
    
    $('#btn_nav_delete').click(function (evn) {
        evn.preventDefault();
        lyq.AjaxUtlis.submitAndRefresh(this.href, {id:list.dataForSelectedRow().id});
    });
});
</script>
<?=JsUtils::ob_end();?>