<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">商品列表</a>
        <ul class="nav nav-pills">
            <li><a href="<?=$this->buildUrl('ajaxadd')?>" id="btn_add">增加</a>
            <li><a href="<?=$this->buildUrl('ajaxdelete')?>" id="btn_nav_delete">删除</a></li>
        </ul>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">商品名称</th>
                        <th width="200">所属品牌</th>
                        <th width="200">分类</th>
                        <th>描述</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr>
                            <td><input type="checkbox" d='{"id":"1","name":"卸妆","brand":"Melvita","category":"卸妆","desc":"好用的卸妆"}'></td>
                            <td>Melvita卸妆水</td>
                            <td>Melvita</td>
                            <td>卸妆</td>
                            <td>好用的卸妆</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:600px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"></h3>
    </div>
    <div class="modal-body">
        <form name="dialog_form" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="name">商品名称</label>
                <div class="controls">
                    <input type="text" id="name" placeholder="商品名称">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="brand">所属品牌</label>
                <div class="controls">
                    <input type="text" id="brand" placeholder="所属品牌">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="category">所属类别</label>
                <div class="controls">
                    <input type="text" id="category" placeholder="所属类别">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="desc">所属类别</label>
                <div class="controls">
                    <textarea rows="3" id="desc" placeholder="介绍"></textarea>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary">确定</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
	var form = document.forms['dialog_form'];
	
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				$('#myModalLabel').html('修改商品属性');
				
				var obj = list.dataForSelectedRow();
				form['name'].value = obj.name;
				form['brand'].value = obj.brand;
				form['category'].value = obj.category;
				form['desc'].value = obj.desc;
                dialog.modal('show');
            }
        }
    });
	
	var dialog = $('#dialog').modal('hide');
	
	$('#btn_add').click(function (evn) {
		evn.preventDefault();
		
		$('#myModalLabel').html('新增商品');
				
		form.reset();
		dialog.modal('show');
	});
    
    $('#btn_nav_delete').click(function (evn) {
        evn.preventDefault();
        lyq.AjaxUtlis.submitAndRefresh(this.href, {id:list.dataForSelectedRow().id});
    });
});
</script>
<?=JsUtils::ob_end();?>