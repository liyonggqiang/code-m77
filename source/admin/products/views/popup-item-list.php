<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">产品列表</a>
        <p class="nav pull-right">
            <button class="btn btn-primary" id="btnSubmit">提交</button>
            <button class="btn btn-danger" id="btnClose">关闭</button>
        </p>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">产品名称</th>
                        <th width="150">规格</th>
                        <th width="100">售价</th>
                        <th width="200">所属商品系列</th>
                        <th width="50">套装?</th>
                        <th width="100">产地</th>
                        <th width="350">备注</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr>
                            <td><input type="checkbox" d='{"id":"1","name":"契尔氏白泥面膜","model":"50ml","price":"120.00","pur_price":"120.00","pur_price_rmb":"96.00","unit":"瓶"}'></td>
                            <td>契尔氏白泥面膜瓶装</td>
                            <td>50ml 瓶装</td>
                            <td>¥205</td>
                            <td>契尔氏白泥面膜</td>
                            <td>N</td>
                            <td>美国</td>
                            <td>-</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
	DialogEx.initClientHandle();
	
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				DialogEx.returnData(list.dataForSelectedRow());
				window.close();
            }
        }
    });
	
	$('#btnClose').click(function () {
		window.close();
	});
});
</script>
<?=JsUtils::ob_end();?>