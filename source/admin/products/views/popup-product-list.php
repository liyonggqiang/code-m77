<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">产品列表</a>
        <p class="nav pull-right">
            <button class="btn btn-primary" type="button">提交</button>
            <button class="btn btn-danger" type="button">关闭</button>
        </p>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">系列名称</th>
                        <th width="150">所属品牌</th>
                        <th width="100">分类</th>
                        <th>描述</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr>
                            <td><input type="checkbox" d='{"id":"1","name":"Melvita卸妆水","brand":"Melvita","category":"卸妆","memo":"好用的卸妆"}'></td>
                            <td>Melvita卸妆水</td>
                            <td>Melvita</td>
                            <td>卸妆</td>
                            <td>好用的卸妆</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
	DialogEx.initClientHandle();
	
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				DialogEx.returnData(list.dataForSelectedRow());
				window.close();
            }
        }
    });
});
</script>
<?=JsUtils::ob_end();?>