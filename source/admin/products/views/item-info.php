<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">新增产品</a>
        <ul class="nav nav-pills">
            <li><a href="#" id="btnSave">保存</a></li>
            <li><a href="#" id="btnDelete">删除</a></li>
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">变更状态<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">设置为可用</a></li>
                  <li><a href="#">设置为不可用</a></li>
                </ul>
            </li>
            <li><a href="#" id="btnBack">返回</a></li>
        </ul>
    </div>
</div>
<form name="frm_info" method="post">
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tr>
            <td><dl class="dl-horizontal">
                    <dt>
                        <label>产品名</label>
                    </dt>
                    <dd>
                        <input class="input-large" type="text" placeholder="产品名">
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                <dt>
                    <label>规格</label>
                </dt>
                <dd>
                    <div >
                        <input class="input-medium" type="text" placeholder="规格">
                    </div>
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>单位</label>
                    </dt>
                    <dd>
                        <input class="input-medium" type="text" placeholder="单位">
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>所属系列</label>
                    </dt>
                    <dd>
                        <div class="input-append">
                            <input class="input-medium" type="text" name="product" placeholder="所属系列" readonly>
                            <input type="hidden" name="product_id">
                            <span class="btn add-on" id="btnSelecProduct" link="<?=$this->buildUrl('popuplist','product','products')?>">
                                  <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                        </div>
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>所属品牌</label>
                    </dt>
                    <dd>
                        <input class="input-medium" type="text" name="brand" placeholder="所属品牌" readonly>
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>所属分类</label>
                    </dt>
                    <dd>
                        <input class="input-medium" type="text" name="category" placeholder="所属分类" readonly>
                    </dd>
                </dl>
            </td>
        </tr>
        
        <tr>
            <td><dl class="dl-horizontal">
                    <dt>
                        <label>产地</label>
                    </dt>
                    <dd>
                        <div class="input-append">
                            <input class="input-medium" type="text" name="country" placeholder="产地" readonly>
                            <input type="hidden" name="country_id">
                            <span class="btn add-on" id="btnSelecCountry" link="<?=$this->buildUrl('popuplist','country','settings')?>">
                            	<i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                        </div>
                    </dd>
                </dl></td>
            <td></td>
            <td>
            	<dl class="dl-horizontal">
                <dt>
                    
                </dt>
                <dd>
                    <div>
                        <label class="checkbox"><input type="checkbox" id="btn_site"> 套装？</label>
                    </div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>售价</label>
                    </dt>
                    <dd>
                        <input class="input-medium" type="text" name="price" placeholder="售价">
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>币种</label>
                    </dt>
                    <dd>
                    	
                        
                        
                        <div class="input-append">
                        	<input type="hidden" name="currency_id">
                            <input class="input-medium" type="text" name="currency" placeholder="币种" readonly>
                            <span class="btn add-on" id="btnSelecCurrency" link="<?=$this->buildUrl('popuplist','currency','settings')?>">
                                  <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                        </div>
                    </dd>
                </dl>
            </td>
            <td></td>
        </tr>
        <tr>
        	<td colspan="3">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>条码</label>
                    </dt>
                    <dd>
                        <div>
                            <input class="input-xxlarge" type="text" placeholder="条码">
                        </div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td colspan="3">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>备注</label>
                    </dt>
                    <dd>
                        <div>
                            <textarea rows="3" class="input-xxlarge" placeholder="500字以内"></textarea>
                        </div>
                    </dd>
                </dl>
            </td>
        </tr>
    </table>
    
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active">
                <a href="#tab1" data-toggle="tab">套装内产品</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="300">产品名</th>
                                    <th width="300">规格</th>
                                    <th width="300">数量</th>
                                    <th>备注</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php for ($i=0; $i<10; $i++):?>
                                <tr>
                                    <td><input type="checkbox" value="29" name="id[]"></td>
                                    <td>化妆包</td>
                                    <td>手袋</td>
                                    <td>1个</td>
                                    <td>专柜送的礼品</td>
                                </tr>
                                <?php endfor;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	var form = document.forms['frm_info'];
	
		//---------------------------------------------------------------------------
	var country_popup_win = new DialogEx($('#btnSelecCountry').attr('link'), {
		features:'width=1000,height=600',
		events:{
			'data':function (evn, data) {
				form['country'].value = data.name;
				form['country_id'].value = data.id;
			}
		}
	});
	
	$('#btnSelecCountry').click(function (evn) {
		country_popup_win.openModal();
	});
	
		//---------------------------------------------------------------------------
	var prod_popup_win = new DialogEx($('#btnSelecProduct').attr('link'), {
		features:'width=1000,height=600',
		events:{
			'data':function (evn, data) {
				form['product'].value = data.name;
				form['product_id'].value = data.id;
				
				form['brand'].value = data.brand;
				form['category'].value = data.category;
			}
		}
	});
	
	$('#btnSelecProduct').click(function (evn) {
		prod_popup_win.openModal();
	});
	
		//---------------------------------------------------------------------------
	var currency_popup_win = new DialogEx($('#btnSelecCurrency').attr('link'), {
		features:'width=1000,height=600',
		events:{
			'data':function (evn, data) {
				form['currency'].value = data.name;
				form['currency_id'].value = data.id;
			}
		}
	});
	
	$('#btnSelecCurrency').click(function (evn) {
		currency_popup_win.openModal();
	});
	
		//---------------------------------------------------------------------------
	var slave = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				alert(this);
			}
		}	
	});
	
	var btn_site = $('#btn_site');
	function display_tabs ()
	{
		if (btn_site[0].checked)
		{
			$('#tabs1').show();
		}
		else
		{
			$('#tabs1').hide();
		}
	}
	btn_site.bind('click', display_tabs);
	display_tabs();
	
		//---------------------------------------------------------------------------
	$('#btnSave').click(function (evn) {
		evn.preventDefault();
	});
	
	$('#btnDelete').click(function (evn) {
		evn.preventDefault();
	});
	
	$('#btnBack').click(function (evn) {
		evn.preventDefault();
		window.history.back();
	});
});
</script>
<?=JsUtils::ob_end();?>