<?php
/**
 * Description of PurchaseorderController
 *
 * @author LYQ <coderfly@163.com>
 */
class Sales_PurchaseorderController extends BaseController
{
    public function listAction ()
    {
        $this->render('purchase-order-list');
    }

    public function newAction ()
    {
        $this->render('purchase-order-new');
    }
	
	public function editAction ()
    {
        $this->render('purchase-order-info');
    }
}
