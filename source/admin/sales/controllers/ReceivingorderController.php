<?php

/**
 * Description of Receivingorder
 *
 * @author LYQ <coderfly@163.com>
 */
class Sales_ReceivingorderController extends BaseController
{
    public function listAction ()
    {
        $this->render('rece-order-list');
    }

    public function newAction ()
    {
        $this->render('rece-order-new');
    }

    public function addAction ()
    {
        
    }

    public function editAction ()
    {
        $this->render('rece-order-info');
    }

    public function updateAction ()
    {
        
    }
}
