<?php

/**
 * Description of SalesorderController
 *
 * @author LYQ <coderfly@163.com>
 */
class Sales_SalesorderController extends BaseController
{
    public function listAction ()
    {
        $this->render('sales-order-list');
    }

    public function newAction ()
    {
        $this->render('sales-order-info');
    }

    public function addAction ()
    {
        
    }

    public function editAction ()
    {
        $this->render('sales-order-info');
    }

    public function updateAction ()
    {
        
    }
}
