<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">收货单详情</a>
        <ul class="nav nav-pills">
            <li><a href="#">保存</a></li>
            <li><a href="#">删除</a></li>
            <li><a href="#">返回</a></li>
        </ul>
    </div>
</div>
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tr>
        	<td colspan="3">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>单号</label>
                    </dt>
                    <dd>
                        <div><input class="input-large" type="text" placeholder="单号"></div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>收货日期</label>
                    </dt>
                    <dd>
                        <div class="input-append" id="dp1">
                        	<input class="input-medium" type="text" placeholder="收货日期">
                        	<span class="btn add-on">
                              <i class="fa fa-calendar" data-date-icon="fa fa-calendar" data-time-icon="fa fa-time"></i>
                            </span>
                        </div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td colspan="3">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>备注</label>
                    </dt>
                    <dd>
                        <div><textarea rows="3" class="input-xxlarge" placeholder="500字以内"></textarea></div>
                    </dd>
                </dl>
            </td>
        </tr>
    </table>
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active">
                <a href="#tab1" data-toggle="tab">订单产品列表</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="200" slv_field="name">品名</th>
                                    <th width="150" slv_field="model">规格</th>
                                    <th width="50" slv_field="unit">单位</th>
                                    <th width="100" slv_field="price">标准售价</th>
                                    <th width="100" slv_field="pur_price">采购价</th>
                                    <th width="100" slv_field="pur_price_rmb">采购价</th>
                                    <th width="50" slv_field="count">数量</th>
                                    <th width="100" slv_field="total_price">总价</th>
                                    <th width="500" slv_field="memo">备注</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php for ($i=0; $i<10; $i++):?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>化妆包</td>
                                    <td>15寸</td>
                                    <td>个</td>
                                    <td>139.00</td>
                                    <td>100.00</td>
                                    <td>90.00</td>
                                    <td>2</td>
                                    <td>200.00</td>
                                    <td>专柜送的礼品</td>
                                </tr>
                                <?php endfor;?>
                            </tbody>
                        </table>
                    </div>
                    <div class="footer">
                    	<div class="pagination pagination-right pagination-small">总价格: <span>7777.00 RMB</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="dia">
    <form name="dialog_form" class="form-horizontal">
    	<input type="hidden" name="model" />
        <input type="hidden" name="unit" />
        <div class="control-group">
            <label class="control-label" for="name">品名</label>
            <div class="controls">
                <div class="input-append">
                    <input class="input-large" type="text" id="name" name="name" placeholder="选取商品">
                    <span class="btn add-on" id="btnSelectItem" link="<?=$this->buildUrl('popuplist','item','products')?>">
                          <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                    </span>
                </div>
                <span class="help-block"><input class="input-large text" type="text" name="info" placeholder="商品说明。" readonly></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">标准售价</label>
            <div class="controls">
                <input type="text" name="price" placeholder="标准售价" readonly />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="pur_price">采购价</label>
            <div class="controls">
                <input type="number" id="pur_price" name="pur_price" step="0.1" placeholder="采购价" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="exchange_rate">当日汇率</label>
            <div class="controls">
                <input type="number" id="exchange_rate" name="exchange_rate" min="0.1" max="99.0" step="0.1" placeholder="当日汇率" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="pur_price_rmb">采购价 RMB</label>
            <div class="controls">
                <input type="number" id="pur_price_rmb" name="pur_price_rmb" step="0.1" placeholder="采购价RMB" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="count">数量</label>
            <div class="controls">
                <input type="number" id="count" name="count" placeholder="数量" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="total_price">总价 RMB</label>
            <div class="controls">
                <input type="number" id="total_price" name="total_price" step="0.1" placeholder="总价 RMB" rvalue="#pur_price_rmb*#count" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="memo">备注</label>
            <div class="controls">
                <textarea rows="3" class="input-xlarge" id="memo" name="memo" placeholder="500字以内"></textarea>
            </div>
        </div>
    </form>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.RelateFields.parse();
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	$('#dp1').datetimepicker({
		pickTime: false
    });
	
		//---------------------------------------------------------------------------
	var form = document.forms['dialog_form'];
	$(form).bind('submit', function (evn) {
		evn.preventDefault();
	});
	
		//---------------------------------------------------------------------------
	var item_popup_win = new DialogEx($('#btnSelectItem').attr('link'), {
		features:'width=1000,height=600',
		events:{
			'data':function (evn, data) {
				form['info'].value = '规格：'+data.model;
				form['name'].value = data.name;
				form['price'].value = data.price;
				form['exchange_rate'].value = 0.8;
				form['pur_price'].value = data.pur_price;
				form['pur_price_rmb'].value = data.pur_price_rmb;
				form['model'].value = data.model;
				form['unit'].value = data.unit;
			}
		}
	});
	
	$('#btnSelectItem').click(function (evn) {
		item_popup_win.openModal();
	});
	
		//---------------------------------------------------------------------------
	var list = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				begin_edit();
			}
		},
		slave_table: {
			post_name:'std'	
		},
		contextmenu:{
			'添加商品':function () {
				form.reset();
				bootbox.dialog({
					message: $('#dia'),
					title: "添加商品",
					buttons: {
						danger: {
							label: "取消",
							className: "btn-danger",
							callback: function() {
								
							}
						},
						success: {
							label: "确定",
							className: "btn-success",
							callback: function() {
								list.slaveTable.addRow({
									'name':form['name'].value,
									'model':form['model'].value,
									'unit':form['unit'].value,
									'price':form['price'].value,
									'pur_price':form['pur_price'].value,
									'pur_price_rmb':form['pur_price_rmb'].value,
									'count':form['count'].value,
									'total_price':form['total_price'].value,
									'memo':form['memo'].value
								});
							}
						}
					}
				});
			},
			'修改选择':function () {
				begin_edit();
			},
			'删除选中':function () {
				list.slaveTable.removeRow(list.slaveTable.selectedRowIndex());
			}
		}
	});
	
	function begin_edit ()
	{
		var idx = list.slaveTable.selectedRowIndex();
		if (-1 == idx)
		{
			bootbox.alert("请选择需要编辑的行。");
			return;	
		}
		
		var data = list.slaveTable.dataForSelectedRow();
		
		form['info'].value = '规格：'+data.model.data;
		form['name'].value = data.name.data;
		form['price'].value = data.price.data;
		form['exchange_rate'].value = 0.8;
		form['pur_price'].value = data.pur_price.data;
		form['pur_price_rmb'].value = data.pur_price_rmb.data;
		form['count'].value = data.count.data;
		form['total_price'].value = data.total_price.data;
		form['model'].value = data.model.data;
		form['unit'].value = data.unit.data;
		form['memo'].value = data.memo.data;
		
		bootbox.dialog({
			message: $('#dia'),
			title: "修改商品",
			buttons: {
				danger: {
					label: "取消",
					className: "btn-danger",
					callback: function() {
						
					}
				},
				success: {
					label: "确定",
					className: "btn-success",
					callback: function() {
						list.slaveTable.updateRow(list.slaveTable.selectedRowIndex(), {
							'name':form['name'].value,
							'model':form['model'].value,
							'unit':form['unit'].value,
							'price':form['price'].value,
							'pur_price':form['pur_price'].value,
							'pur_price_rmb':form['pur_price_rmb'].value,
							'count':form['count'].value,
							'total_price':form['total_price'].value,
							'memo':form['memo'].value
						});
					}
				}
			}
		});
	}
});
</script>
<?=JsUtils::ob_end();?>