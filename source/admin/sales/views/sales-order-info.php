<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">新增销售订单</a>
        <ul class="nav nav-pills">
            <li><a href="#">保存</a></li>
            <li><a href="#">删除</a></li>
            <li><a href="#">返回</a></li>
        </ul>
    </div>
</div>
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
    	<tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>单号</label>
                    </dt>
                    <dd>
                        <div><input class="input-large" type="text" placeholder="单号"></div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>客户名称</label>
                    </dt>
                    <dd>
                        <div class="input-append">
                            <input class="input-medium" type="text" placeholder="客户名称">
                            <span class="btn add-on" id="btnSelectUsers" link="<?=$this->buildUrl('popuplist','customer','crm')?>">
                                  <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                            <span class="btn add-on" style="color:red">
                                  <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                            </span>
                        </div>
                        <span class="help-block" id="cust_info">请选择客户。</span>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>备注</label>
                    </dt>
                    <dd>
                        <div><textarea rows="3" class="input-xxlarge" placeholder="500字以内"></textarea></div>
                    </dd>
                </dl>
            </td>
        </tr>
    </table>
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active"><a href="#tab1" data-toggle="tab">订单产品列表</a></li>
            <li><a href="#tab2" data-toggle="tab">发货地址</a></li>
            <li><a href="#tab3" data-toggle="tab">物流单</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="200" slv_field="name">产品名</th>
                                    <th width="100" slv_field="model">规格</th>
                                    <th width="50" slv_field="unit">单位</th>
                                    <th width="120" slv_field="per_price">单价</th>
                                    <th width="50" slv_field="count">数量</th>
                                    <th width="120" slv_field="per_price">预售价</th>
                                    <th width="120" slv_field="pur_price">采购价</th>
                                    <th width="120" slv_field="pur_rate">采购汇率</th>
                                    <th width="120" slv_field="act_price">售价</th>
                                    <th width="120" slv_field="act_rate">卖出汇率</th>
                                    <th width="120" slv_field="total_price">总价</th>
                                    <th width="500" slv_field="memo">备注</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php for ($i=0; $i<10; $i++):?>
                                <tr>
                                    <td><input type="checkbox" value="29" name="id[]"></td>
                                    <td>化妆包</td>
                                    <td>手袋</td>
                                    <td>个</td>
                                    <td>90 HKD</td>
                                    <td style="text-align:right">1</td>
                                    <td>100.00</td>
                                    <td>90.00</td>
                                    <td>0.8</td>
                                    <td>100.00</td>
                                    <td>0.9</td>
                                    <td>100.00</td>
                                    <td>专柜送的礼品</td>
                                </tr>
                                <?php endfor;?>
                            </tbody>
                        </table>
                    </div>
                    <div class="footer">
                    	<div class="pagination pagination-right pagination-small">总价格: <span>7777.00 RMB</span></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2">
            	<table class="table table-noborder table-striped table-condensed">
                	<tr>
                    	<td>李生</td>
                        <td>XX省xx市xx区xx路323号 16楼1606室</td>
                        <td>130123456789</td>
                        <td>123456</td>
                    </tr>
                </table>
            </div>
            <div class="tab-pane" id="tab3">
            	<table class="table table-noborder table-striped table-condensed">
                	<tr>
                    	<td>顺丰速运</td>
                        <td>420510000000000</td>
                        <td>2014-08-05 已发货</td>
                        <td><a href="#">快件查询</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:800px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">title</h3>
    </div>
    <div class="modal-body">
        <form name="dialog_form" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="name">品名</label>
                <div class="controls">
                    <div class="input-append">
                        <input class="input-large" type="text" id="name" name="name" placeholder="客户名称">
                        <span class="btn add-on" id="btnSelectUsers" link="<?=$this->buildUrl('popuplist','customer','crm')?>">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                    <span class="help-block" id="cust_info">商品说明。</span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="per_price">预售价</label>
                <div class="controls">
                    <input type="text" id="per_price" name="per_price" placeholder="预售价">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="pur_price">采购价</label>
                <div class="controls">
                    <input type="text" id="pur_price" name="pur_price" placeholder="采购价">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="act_price">卖出价</label>
                <div class="controls">
                    <input type="text" id="act_price" name="pur_price" placeholder="卖出价">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="act_reate">卖出汇率</label>
                <div class="controls">
                    <input type="text" id="act_rate" name="act_reate" placeholder="卖出汇率">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="count">数量</label>
                <div class="controls">
                    <input type="text" id="count" name="count" placeholder="数量">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="total_price">总价</label>
                <div class="controls">
                    <input type="text" id="total_price" name="total_price" placeholder="总价">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">备注</label>
                <div class="controls">
                    <textarea rows="3" class="input-xlarge" placeholder="500字以内"></textarea>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary">提交</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	var dialog = $('#dialog').modal('hide');
	var form = document.forms['dialog_form'];
	$(form).bind('submit', function (evn) {
		evn.preventDefault();
		document.title = "x";
	});
	
	var cust_popup_win = new DialogEx($('#btnSelectUsers').attr('link'), {
		features:'width=1000,height=600',
		events:{
			'data':function (evn, data) {
				
			}
		}
	});
	$('#btnSelectUsers').click(function (evn) {
		cust_popup_win.openModal();
	});
	
	var dialog = $('#dialog').modal('hide');
	
	var list = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				
			}
		},
		slave_table: {
			post_name:'std'	
		},
		contextmenu:{
			'添加商品':function () {
				dialog['flag'] = 'add';
				
				$('#myModalLabel').html('添加商品');
				form.reset();
				dialog.modal('show');
			},
			'修改选择':function () {
				dialog['flag'] = 'edit';
				
				$('#myModalLabel').html('修改商品');
				
				var obj = list.slaveTable.dataForSelectedRow();
				form['name'].value = obj.name.data;
				form['per_price'].value = obj.per_price.data;
				form['pur_price'].value = obj.pur_price.data;
				form['count'].value = obj.count.data;
				form['total_price'].value = obj.total_price.data;
				
				dialog.modal('show');
			},
			'删除选中':function () {
				list.slaveTable.removeRow(list.slaveTable.selectedRowIndex());
			}
		}
	});
});
</script>
<?=JsUtils::ob_end();?>