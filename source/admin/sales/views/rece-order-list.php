<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">收货单列表</a>
        <ul class="nav nav-pills">
            <li><a href="<?=$this->buildUrl('new')?>">增加</a>
        </ul>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">单号</th>
                        <th width="200">收货日期</th>
                        <th width="150">总价</th>
                        <th width="100">状态</th>
                        <th>备注</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr link="<?=$this->buildUrl('edit',null,null,array('id'=>1))?>">
                            <td><input type="checkbox" d='1'></td>
                            <td>2014080600001</td>
                            <td>2014-08-06</td>
                            <td>¥ 7890.00</td>
                            <td><span class="label label-success">已入库</span></td>
                            <td>-</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				window.location = $(this).attr('link');
            }
        }
    });
});
</script>
<?=JsUtils::ob_end();?>