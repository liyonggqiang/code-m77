<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">新增采购订单</a>
        <ul class="nav nav-pills">
            <li><a href="#">保存</a></li>
            <li><a href="#">放弃</a></li>
            <li><a href="#">返回</a></li>
        </ul>
    </div>
</div>
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>单号</label>
                    </dt>
                    <dd>
                        <div><input class="input-large" type="text" placeholder="单号"></div>
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>生成日期</label>
                    </dt>
                    <dd>
                        <div><input class="input-medium" type="text" placeholder="生成日期"></div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>计划采购日期</label>
                    </dt>
                    <dd>
                        <div class="input-append" id="dp1">
                        	<input class="input-medium" type="text" placeholder="计划采购日期">
                        	<span class="btn add-on">
                              <i class="fa fa-calendar" data-date-icon="fa fa-calendar" data-time-icon="fa fa-time"></i>
                            </span>
                        </div>
                    </dd>
                </dl>
            </td>
            <td>
            	<dl class="dl-horizontal">
                    <dt>
                        <label>实际采购日期</label>
                    </dt>
                    <dd>
                        <div class="input-append" id="dp2">
                        	<input class="input-medium" type="text" placeholder="实际采购日期">
                        	<span class="btn add-on">
                              <i class="fa fa-calendar" data-date-icon="fa fa-calendar" data-time-icon="fa fa-time"></i>
                            </span>
                        </div>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td colspan="2">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>备注</label>
                    </dt>
                    <dd>
                        <div><textarea rows="3" class="input-xxlarge" placeholder="500字以内"></textarea></div>
                    </dd>
                </dl>
            </td>
        </tr>
    </table>
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active"><a href="#tab1" data-toggle="tab">订单产品列表</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="300">产品名</th>
                                    <th width="100">品牌</th>
                                    <th width="150">规格</th>
                                    <th width="120">采购价格</th>
                                    <th width="100">当日汇率</th>
                                    <th width="120">采购价格RMB</th>
                                    <th width="50">数量</th>
                                    <th width="120">总价RMB</th>
                                    <th width="350">备注</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="footer">
                    	<div class="pagination pagination-right pagination-small">总价格: <span>7777.00 RMB</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="dia">
    <form name="dialog_form" class="form-horizontal">
        <div class="control-group">
            <label class="control-label" for="name">品名</label>
            <div class="controls">
                <div class="input-append">
                    <input class="input-large" type="text" id="name" name="name" placeholder="选取商品">
                    <span class="btn add-on" id="btnSelectItem" link="<?=$this->buildUrl('popuplist','item','products')?>">
                          <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                    </span>
                </div>
                <span class="help-block" id="info">商品说明。</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="price">官方售价</label>
            <div class="controls">
                <input type="number" id="pur_price" name="price" placeholder="官方售价">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="pur_price">采购价</label>
            <div class="controls">
                <input type="number" id="pur_price" name="pur_price" placeholder="采购价">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="exchange_rate">当日汇率</label>
            <div class="controls">
                <input type="number" id="exchange_rate" name="exchange_rate" min="0.1" max="10.0" step="0.1" placeholder="当日汇率">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="act_price_rmb">采购价 RMB</label>
            <div class="controls">
                <input type="number" id="act_price_rmb" name="act_price_rmb" placeholder="采购价RMB">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="count">数量</label>
            <div class="controls">
                <input type="number" id="count" name="count" placeholder="数量">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="total_price">总价 RMB</label>
            <div class="controls">
                <input type="number" id="total_price" name="total_price" placeholder="总价 RMB" rvalue="#act_price_rmb*#count">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">备注</label>
            <div class="controls">
                <textarea rows="3" class="input-xlarge" placeholder="500字以内"></textarea>
            </div>
        </div>
    </form>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.RelateFields.parse();
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	$('#dp1').datetimepicker({
		pickTime: false
    });
	
	$('#dp2').datetimepicker({
		pickTime: false
    });
	
		//---------------------------------------------------------------------------
	var form = document.forms['dialog_form'];
	
		//---------------------------------------------------------------------------
	var item_popup_win = new DialogEx($('#btnSelectItem').attr('link'), {
		features:'width=1000,height=600',
		events:{
			'data':function (evn, data) {
				$(form).find('span[id=info]').html('规格：'+data.model);
				form['name'].value = data.name;
				form['price'].value = data.price;
				form['exchange_rate'].value = 0.8;
				form['act_price_rmb'].value = data.price_rmb;
			}
		}
	});
	$('#btnSelectItem').click(function (evn) {
		item_popup_win.openModal();
	});
	
		//---------------------------------------------------------------------------
	var list = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				
			}
		},
		slave_table: {
			post_name:'std'	
		},
		contextmenu:{
			'添加商品':function () {
				form.reset();
				bootbox.dialog({
					message: $('#dia'),
					title: "添加商品",
					buttons: {
						danger: {
							label: "取消",
							className: "btn-danger",
							callback: function() {
								
							}
						},
						success: {
							label: "确定",
							className: "btn-success",
							callback: function() {
									
							}
						}
					}
				});
			},
			'修改选择':function () {
				dialog['flag'] = 'edit';
				
				$('#myModalLabel').html('修改商品');
				
				var obj = list.slaveTable.dataForSelectedRow();
				/*
				form['name'].value = obj.name.data;
				form['per_price'].value = obj.per_price.data;
				form['pur_price'].value = obj.pur_price.data;
				form['count'].value = obj.count.data;
				form['total_price'].value = obj.total_price.data;
				*/
				
				dialog.modal('show');
			},
			'删除选中':function () {
				list.slaveTable.removeRow(list.slaveTable.selectedRowIndex());
			}
		}
	});
});
</script>
<?=JsUtils::ob_end();?>