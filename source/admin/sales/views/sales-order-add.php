<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">新增销售订单</a>
        <ul class="nav nav-pills">
            <li><a href="#">保存</a></li>
            <li><a href="#">删除</a></li>
            <li><a href="#">返回</a></li>
        </ul>
    </div>
</div>
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tr>
        	<td colspan="3">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>客户名称</label>
                    </dt>
                    <dd>
                        <div class="input-append">
                            <input class="input-medium" type="text" placeholder="客户名称">
                            <span class="btn add-on" id="btnSelectUsers" link="<?=$this->buildUrl('popuplist','customer','crm')?>">
                                  <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                            <span class="btn add-on" style="color:red">
                                  <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                            </span>
                        </div>
                        <span class="help-block" id="cust_info">请选择客户。</span>
                    </dd>
                </dl>
            </td>
        </tr>
        <tr>
        	<td colspan="3">
            	<dl class="dl-horizontal">
                    <dt>
                        <label>备注</label>
                    </dt>
                    <dd>
                        <div><textarea rows="3" class="input-xxlarge" placeholder="500字以内"></textarea></div>
                    </dd>
                </dl>
            </td>
        </tr>
    </table>
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active">
                <a href="#tab1" data-toggle="tab">订单产品列表</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="200">产品名</th>
                                    <th width="200">规格</th>
                                    <th width="50">数量</th>
                                    <th width="50">单位</th>
                                    <th width="120">预售价</th>
                                    <th width="120">采购价</th>
                                    <th width="120">总价</th>
                                    <th width="500">备注</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php for ($i=0; $i<10; $i++):?>
                                <tr>
                                    <td><input type="checkbox" value="29" name="id[]"></td>
                                    <td>化妆包</td>
                                    <td>手袋</td>
                                    <td style="text-align:right">1</td>
                                    <td>个</td>
                                    <td>100.00</td>
                                    <td>90.00</td>
                                    <td>100.00</td>
                                    <td>专柜送的礼品</td>
                                </tr>
                                <?php endfor;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:800px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">title</h3>
    </div>
    <div class="modal-body">
        <form name="dialog_form" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="name">品名</label>
                <div class="controls">
                    <div class="input-append">
                        <input class="input-medium" type="text" placeholder="客户名称">
                        <span class="btn add-on" id="btnSelectUsers" link="<?=$this->buildUrl('popuplist','customer','crm')?>">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                    <span class="help-block" id="cust_info">商品说明。</span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="code">预售价</label>
                <div class="controls">
                    <input type="text" id="code" placeholder="预售价">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="code">采购价</label>
                <div class="controls">
                    <input type="text" id="code" placeholder="采购价">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="code">数量</label>
                <div class="controls">
                    <input type="text" id="code" placeholder="数量">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="code">总价</label>
                <div class="controls">
                    <input type="text" id="code" placeholder="总价">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary">确定</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	var dialog = $('#dialog').modal('hide');
	var form = document.forms['dialog_form'];
	
	$('#btnSelectUsers').click(function (evn) {
		var dialog = new DialogEx($(this).attr('link'), {features:'width=1000,height=600'});
		dialog.openModal();
	});
	
	var dialog = $('#dialog').modal('hide');
	
	var slave = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				$('#myModalLabel').html('修改商品');
				dialog.modal('show');
			}
		},
		contextmenu:{
			'添加商品':function () {
				$('#myModalLabel').html('添加商品');
				form.reset();
				dialog.modal('show');
			},
			'修改选择':function () {
				$('#myModalLabel').html('修改商品');
				dialog.modal('show');
			},
			'删除选中':function () {
				
			}
		}
	});
});
</script>
<?=JsUtils::ob_end();?>