<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">List</a>
        <ul class="nav">
            <li class="active"><a href="#">首页</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropmenu<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">设置为可用</a></li>
                  <li><a href="#">设置为不可用</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li class="nav-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<div class="fix-box">
	<div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th width="10"><input type="checkbox" id="chk_all"></th>
                    <th width="100">商机编号</th>
                    <th width="100">商机主题</th>
                    <th width="100">顾客编号</th>
                    <th width="100">商机实体</th>
                    <th width="100">客户</th>
                    <th width="100">联系人</th>
                    <th width="100">商机状态</th>
                    <th>重要程度</th>
                </tr>
            	</thead>
            </table>
        </div>
        <div class="body">
        <table class="table table-striped table-hover table-condensed table-bordered">
       		<tbody>
            	<?php for ($i=0; $i<50; $i++):?>
                <tr>
                    <td><input type="checkbox" value='{"id":"1"}'></td>
                    <td>20110126001</td>
                    <td>测试用主题</td>
                    <td>100003</td>
                    <td>客户</td>
                    <td>亿通电缆</td>
                    <td></td>
                    <td><span class="label label-success">准备</span></td>
                    <td><span class="label label-warning">重要</span></td></td>
            	</tr>
                <?php endfor;?>
            </tbody>
        </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="/js/lyq.listEx.js"></script>
<script>
$(function () {
	var list = new lyq.ListEx($('#list'), {
		events: {
			dblclick:function (evn) {
				alert(this);
			}
		}	
	});
});
</script>