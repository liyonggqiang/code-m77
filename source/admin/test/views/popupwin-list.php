<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">List for Popupwin</a>
        <p class="nav pull-right">
            <button class="btn btn-primary" type="button">Submit</button>
            <button class="btn btn-danger" type="button">Close</button>
        </p>
    </div>
</div>

<div class="fix-box">
	<div class="table-list">
        <div class="header" style="overflow: hidden;">
            <table class="table table-striped table-hover table-responsive" style="width: 2000px; min-width: 100%;">
                <thead><tr>
                    <th width="20"><input type="checkbox" id="chk_all"></th>
                    <th width="100">商机编号</th>
                    <th width="100">商机主题</th>
                    <th width="100">顾客编号</th>
                    <th width="100">商机实体</th>
                    <th width="100">客户</th>
                    <th width="100">联系人</th>
                    <th width="100">商机状态</th>
                    <th width="100">重要程度</th>
                </tr>
            </thead></table>
        </div>
    
        <div class="body" style="overflow: auto;">
        <table id="st" class="table table-striped table-hover table-condensed" style="width: 2000px; min-width: 100%;">
                <tbody><tr del="/crmnew/businesseditor/delete/contact_sn//customer_sn//id/29/" style="" _sel_="">
                <td width="20"><input type="checkbox" value="29" name="id[]"></td>
                <td width="100">20110126001</td>
                <td width="100">测试用主题</td>
                <td width="100">100003</td>
                <td width="100">客户</td>
                <td width="100">亿通电缆</td>
                <td width="100"></td>
                <td width="100"><span class="label label-success">准备</span></td>
                <td width="100"><span class="label label-warning">重要</span></td></td>
            </tr>
                <tr del="/crmnew/businesseditor/delete/contact_sn//customer_sn//id/19/" _sel_="1">
                <td width="20"><input type="checkbox" value="19" name="id[]"></td>
                <td width="100">20110119004</td>
                <td width="100">test</td>
                <td width="100"></td>
                <td width="100">联系人</td>
                <td width="100"></td>
                <td width="100">联系人</td>
                <td width="100">准备</td>
                <td width="100">非常重要</td>
            </tr>
            </tbody>
        </table>
        </div>
    
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
                <div class="pagination pagination-right pagination-small">
                    <ul>
                        <li>
                            <a href="#">Prev</a>
                        </li>
                        <li>
                            <a href="#">1</a>
                        </li>
                        <li>
                            <a href="#">2</a>
                        </li>
                        <li>
                            <a href="#">3</a>
                        </li>
                        <li>
                            <a href="#">4</a>
                        </li>
                        <li>
                            <a href="#">5</a>
                        </li>
                        <li>
                            <a href="#">Next</a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>
</div>