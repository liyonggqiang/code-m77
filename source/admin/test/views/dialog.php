<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">List</a>
        <ul class="nav">
            <li class="active"><a href="#">Dialog</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
        </ul>
    </div>
</div>
<div class="fix-box padd">
	<div class="well">
    	<a href="#alertDialog" role="button" class="btn" data-toggle="modal">text dialog</a>
    	<a href="#formDialog" role="button" class="btn" data-toggle="modal">form dialog</a>
    </div>
</div>

<div id="alertDialog" class="modal hide" tabindex="-1" role="dialog" style="width:500px">
	<div class="alert alert-block alert-error fade in no-margin">
        <h4 class="alert-heading">Oh snap! You got an error!</h4>
        <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
        <p>
            <a href="#" class="btn btn-danger">Take this action</a>
            <a href="#" class="btn">Or do this</a>
        </p>
    </div>
</div>

<div id="formDialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <table id="st" class="table table-condensed table-noborder" style="width:400px; min-width: 100%;">
        <tbody>
            <tr>
                <td width="100">
                	<label>Label name</label>
                    <div class="input-append">
                    	<input class="span2" id="appendedInputButtons" type="text">
                    	<span class="btn add-on" id="btnSelecItems">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                </td>
                <td width="100">
                	<label>Label name</label>
                    <div class="input-append">
                    	<input class="span2" id="appendedInputButtons" type="text">
                    	<span class="btn add-on" id="btnSelecItems">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                </td>
                <td width="100">
                	<label>Label name</label>
                    <div class="input-append">
                    	<input class="span2" id="appendedInputButtons" type="text">
                    	<span class="btn add-on" id="btnSelecItems">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="100">
                	<label>Label name</label>
                    <div class="input-append">
                    	<input class="span2" id="appendedInputButtons" type="text">
                    	<span class="btn add-on" id="btnSelecItems">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                </td>
                <td width="100">
                	<label>Label name</label>
                    <div class="input-append">
                    	<input class="span2" id="appendedInputButtons" type="text">
                    	<span class="btn add-on" id="btnSelecItems">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                </td>
                <td width="100">
                	<label>Label name</label>
                    <div class="input-append">
                    	<input class="span2" id="appendedInputButtons" type="text">
                    	<span class="btn add-on" id="btnSelecItems">
                              <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                        </span>
                        <span class="btn add-on" style="color:red">
                              <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                        </span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="modal-footer">
    	<a href="#" class="btn btn-primary">Save changes</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    </div>
</div>
<script>

</script>