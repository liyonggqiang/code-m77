<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">主从列表视图</a>
        <ul class="nav nav-pills">
            <li><a href="#">首页</a></li><li class="divider-vertical"></li>
            <li><a href="#">Link</a></li><li class="divider-vertical"></li>
            <li><a href="#">Link</a></li>
        </ul>
    </div>
</div>

<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tbody>
            <tr>
                <td width="100"><label>Label name</label>
                    <div class="input-append">
                    <input class="span2" id="appendedInputButtons" type="text">
                    <button class="btn" type="button">...</button>
                    <button class="btn" type="button">X</button>
                    </div>
                </td>
                <td width="100"><label>Label name</label>
                    <div class="input-prepend input-append">
                    <span class="add-on">$</span>
                    <input class="span2" id="appendedPrependedInput" type="text">
                    <span class="add-on">.00</span>
                    </div>
                </td>
                <td width="100"><label>Label name</label>
                    <div class="input-prepend">
                    <span class="add-on">@</span>
                    <input class="span2" id="prependedInput" type="text" placeholder="Username">
                    </div>
                </td>
            </tr>
            <tr>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-prepend">
                            <span class="add-on">@</span>
                            <input class="span2" id="prependedInput" type="text" placeholder="Username">
                            </div>
                        </dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-append">
                            <input class="span2" id="appendedInputButtons" type="text">
                            <button class="btn" type="button">...</button>
                            <button class="btn" type="button">X</button>
                            </div>
                        </dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-prepend">
                            <span class="add-on">@</span>
                            <input class="span2" id="prependedInput" type="text" placeholder="Username">
                            </div>
                        </dd>
                        </dl>
                </td>
            </tr>
            <tr>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
            </tr>
        </tbody>
    </table>
    
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead><tr>
                    <th width="10"><input type="checkbox" id="chk_all"></th>
                    <th width="100" slv_field="sn">商机编号</th>
                    <th width="100" slv_field="title">商机主题</th>
                    <th width="100" slv_field="cust_sn">顾客编号</th>
                    <th width="100" slv_field="chance">商机实体</th>
                    <th width="100" slv_field="customer">客户</th>
                    <th width="100" slv_field="contact">联系人</th>
                    <th width="100" slv_field="state">商机状态</th>
                    <th width="100" slv_field="imp">重要程度</th>
                </tr>
            </thead></table>
        </div>
        <div class="body">
        <table class="table table-striped table-hover table-condensed">
            <tbody>
            	<?php for ($i=0; $i<50; $i++):?>
                <tr>
                    <td><input type="checkbox" value="29" name="id[]"></td>
                    <td>20110126001</td>
                    <td>测试用主题</td>
                    <td>100003</td>
                    <td>客户</td>
                    <td>亿通电缆</td>
                    <td></td>
                    <td>准备</td>
                    <td>重要</td>
            	</tr>
                <?php endfor;?>
            </tbody>
        </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                <li><a href="#">Prev</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
    
</div>
<script src="/js/lyq.ContextMenu.js"></script>
<script src="/js/lyq.listEx.js"></script>
<script src="/js/lyq.SlaveTable.js"></script>
<script>
$(function () {
	var list = new lyq.ListEx($('#list'), {
		full_size:true,
		mulit_selection:false,
		events: {
			dblclick:function (evn) {
				alert(this);
			}
		},
		slave_table: {
			post_name:'std'	
		},
		contextmenu: {
			'add' : function (obj) {
				list.slaveTable.addRow({
					'sn':'123',
					'title':'aaa',
					'cust_sn':'bbb',
					'chance':'ccc',
					'customer':'ddd',
					'contact':'eee',
					'state':'fff',
					'imp':'ggg'
				});
				
			},
			'update' : function () {
				list.slaveTable.updateRow(list.selectedIndex(), {
					'sn':'111',
					'title':'AAA',
					'cust_sn':'BBB',
					'chance':'CCC',
					'customer':'DDD',
					'contact':'EEE',
					'state':'FFF',
					'imp':'GGG'
				});
			},
			'delete' : function () {
				list.slaveTable.removeRow(list.selectedIndex());
			}
		}
	});
});
</script>