<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Tabs视图</a>
        <ul class="nav">
            <li class="active"><a href="#">首页</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
        </ul>
    </div>
</div>

<div class="padd fix-box">
    <table id="st" class="table table-condensed table-noborder" style="width: 2000px; min-width: 100%;">
            <tr>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-prepend">
                            <span class="add-on">@</span>
                            <input class="span2" id="prependedInput" type="text" placeholder="Username">
                            </div>
                            <span class="help-block">Example block-level.</span>
                        </dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-append">
                            <input class="span2" id="appendedInputButtons" type="text">
                            <button class="btn" type="button">...</button>
                            <button class="btn" type="button">X</button>
                            </div>
                        </dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-prepend">
                            <span class="add-on">@</span>
                            <input class="span2" id="prependedInput" type="text" placeholder="Username">
                            </div>
                        </dd>
                        </dl>
                </td>
            </tr>
            <tr>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-append" id="datetimepicker1">
                                <input type="text" class="form-control dtpicker" data-format="yyyy-MM-dd">
                                <span class="btn add-on">
                                  <i class="fa fa-calendar" data-date-icon="fa fa-calendar" data-time-icon="fa fa-time"></i>
                                </span>
                            </div>
                        </dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
            </tr>
    </table>
    
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active"><a href="#tab1" data-toggle="tab">List</a></li>
            <li><a href="#tab2" data-toggle="tab">Text</a></li>
            <li><a href="#tab3" data-toggle="tab">Text</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead><tr>
                                <th width="10"><input type="checkbox" id="chk_all"></th>
                                <th width="100">商机编号</th>
                                <th width="100">商机主题</th>
                                <th width="100">顾客编号</th>
                                <th width="100">商机实体</th>
                                <th width="100">客户</th>
                                <th width="100">联系人</th>
                                <th width="100">商机状态</th>
                                <th width="100">重要程度</th>
                            </tr>
                        </thead></table>
                    </div>
                    <div class="body">
                    <table class="table table-striped table-hover table-condensed">
                        <tbody>
                            <?php for ($i=0; $i<50; $i++):?>
                            <tr>
                            <td><input type="checkbox" value="29" name="id[]"></td>
                            <td>20110126001</td>
                            <td>测试用主题</td>
                            <td>100003</td>
                            <td>客户</td>
                            <td>亿通电缆</td>
                            <td></td>
                            <td>准备</td>
                            <td>重要</td>
                            </tr>
                            <?php endfor;?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2">
                <p>Howdy, I'm in Section 2.</p>
            </div>
            <div class="tab-pane" id="tab3">
                <p>Howdy, I'm in Section 2.</p>
            </div>
        </div>
    </div>
    
</div>
<script src="/js/lyq.listEx.js"></script>
<script src="/js/lyq.TabEx.js"></script>
<script>
$(function () {
	$('#datetimepicker1').datetimepicker({
      pickTime: false
    });
	
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	var slave = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				alert(this);
			}
		}	
	});
});
</script>