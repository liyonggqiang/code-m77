<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Popupwin</a>
        <ul class="nav">
            <li class="active"><a href="#">首页</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
        </ul>
    </div>
</div>

<div class="padd">
    <table id="st" class="table table-condensed table-noborder" style="width: 2000px; min-width: 100%;">
        <tbody>
            <tr>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Popup win#1</label></dt>
                        <dd>
                        	<div class="input-append">
                            <input class="span2" id="appendedInputButtons" type="text">
                            <span class="btn add-on" id="btnSelecItems" link="<?=$this->buildUrl('popuplist')?>">
                                  <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                            <span class="btn add-on" style="color:red">
                                  <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                            </span>
                            </div>
                        </dd>
                        </dl>
                </td>
                <td width="100" colspan="2">
                	    <dl class="dl-horizontal">
                        <dt><label>Popup win#1</label></dt>
                        <dd>
                        	<div class="input-append">
                            <input class="span2" id="appendedInputButtons" type="text">
                            <span class="btn add-on">
                                  <i class="fa fa-search" data-date-icon="fa fa-search"></i>
                            </span>
                            <span class="btn add-on" style="color:red">
                                  <i class="fa fa-times" data-date-icon="fa fa-times"></i>
                            </span>
                            </div>
                        </dd>
                        </dl>
                </td>
            </tr>
            <tr>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd>
                        	<div class="input-append" id="datetimepicker1">
                                <input type="text" class="form-control dtpicker" data-format="yyyy-MM-dd">
                                <span class="btn add-on">
                                  <i class="fa fa-calendar" data-date-icon="fa fa-calendar" data-time-icon="fa fa-time"></i>
                                </span>
                            </div>
                        </dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
                <td width="100">
                	    <dl class="dl-horizontal">
                        <dt><label>Label name</label></dt>
                        <dd><input type="text" placeholder="Type something…"></dd>
                        </dl>
                </td>
            </tr>
        </tbody>
    </table>
    
    <div class="tabbable">
        <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab1" data-toggle="tab">Section 1</a>
            </li>
            <li>
                <a href="#tab2" data-toggle="tab">Section 2</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <table id="st" class="table table-striped table-hover table-condensed" style="width: 2000px; min-width: 100%;">
                	<thead><tr>
                            <th width="20"><input type="checkbox" id="chk_all"></th>
                            <th width="100">商机编号</th>
                            <th width="100">商机主题</th>
                            <th width="100">顾客编号</th>
                            <th width="100">商机实体</th>
                            <th width="100">客户</th>
                            <th width="100">联系人</th>
                            <th width="100">商机状态</th>
                            <th width="100">重要程度</th>
                        </tr>
                    </thead>
                	<tbody><tr del="/crmnew/businesseditor/delete/contact_sn//customer_sn//id/29/" style="" _sel_="">
                        <td width="20"><input type="checkbox" value="29" name="id[]"></td>
                        <td width="100">20110126001</td>
                        <td width="100">测试用主题</td>
                        <td width="100">100003</td>
                        <td width="100">客户</td>
                        <td width="100">亿通电缆</td>
                        <td width="100"></td>
                        <td width="100">准备</td>
                        <td width="100">重要</td>
                    </tr>
                        <tr del="/crmnew/businesseditor/delete/contact_sn//customer_sn//id/19/" _sel_="1">
                        <td width="20"><input type="checkbox" value="19" name="id[]"></td>
                        <td width="100">20110119004</td>
                        <td width="100">test</td>
                        <td width="100"></td>
                        <td width="100">联系人</td>
                        <td width="100"></td>
                        <td width="100">联系人</td>
                        <td width="100">准备</td>
                        <td width="100">非常重要</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab2">
                <p>Howdy, I'm in Section 2.</p>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript" src="/js/lyq.DialogEx.js"></script>
<script>
$(function () {
	$('#datetimepicker1').datetimepicker({
      pickTime: false
    });
	
	$('#btnSelecItems').click(function (evn) {
		var dialog = new DialogEx($(this).attr('link'), {features:'width=1000,height=600'});
		dialog.openModal();
	});
});
</script>