<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestController
 *
 * @author LYQ
 */
class Test_TestController extends BaseController
{
    public function listAction ()
    {
        $this->render('list');
    }
    
    public function slaveAction ()
    {
        $this->render('slave');
    }

    public function tabsAction ()
    {
        $this->render('tabs');
    }
    
    public function popupwinAction ()
    {
        $this->render('popupwin');
    }
    
    public function popuplistAction ()
    {
        $this->render('popupwin-list');
    }
	
	public function dialogAction ()
	{
		$this->render('dialog');
	}
}
