<?php
/**
 * Description of CustomerController
 *
 * @author LYQ <coderfly@163.com>
 */
class Crm_CustomerController extends BaseController
{
    public function listAction ()
    {
        $this->render('customer-list');
    }

    public function newAction ()
    {
        $this->render('customer-info');
    }

    public function addAction ()
    {
        
    }

    public function editAction ()
    {
        $this->render('customer-info');
    }

    public function updateAction ()
    {
        
    }
    
    public function popuplistAction ()
    {
        $this->render('popup-customer-list');
    }
}
