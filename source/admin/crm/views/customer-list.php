<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">客户列表</a>
        <ul class="nav nav-pills">
            <li><a href="<?=$this->buildUrl('new')?>">增加</a>
            <li><a href="<?=$this->buildUrl('ajaxdelete')?>" id="btn_nav_delete">删除</a></li>
        </ul>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">昵称</th>
                        <th width="200">来自</th>
                        <th width="200">联系方式</th>
                        <th>备注</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr link="<?=$this->buildUrl('edit',null,null,array('id'=>1))?>">
                            <td><input type="checkbox" d='{"id":"1"}'></td>
                            <td>小哈哈</td>
                            <td>淘宝</td>
                            <td>旺旺1234567</td>
                            <td>大客户</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
	var form = document.forms['dialog_form'];
	
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				window.location = $(this).attr('link');
            }
        }
    });
    
    $('#btn_nav_delete').click(function (evn) {
        evn.preventDefault();
        lyq.AjaxUtlis.submitAndRefresh(this.href, {id:list.dataForSelectedRow().id});
    });
});
</script>
<?=JsUtils::ob_end();?>