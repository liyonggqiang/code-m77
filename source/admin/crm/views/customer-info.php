<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">客户信息</a>
        <ul class="nav nav-pills">
            <li><a href="#">保存</a></li>
            <li><a href="#">删除</a></li>
            <li><a href="#">返回</a></li>
        </ul>
    </div>
</div>
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tr>
            <td colspan="2"><dl class="dl-horizontal">
                    <dt>
                        <label>昵称</label>
                    </dt>
                    <dd>
                        <div class="input-prepend">
                            <input class="span2" type="text" placeholder="昵称">
                        </div>
                    </dd>
                </dl></td>
        </tr>
        <tr>
            <td width="400"><dl class="dl-horizontal">
                    <dt>
                        <label>来自</label>
                    </dt>
                    <dd>
                        <div class="input-prepend">
                            <input class="span2" type="text" placeholder="QQ/淘宝/微信">
                        </div>
                    </dd>
                </dl></td>
            <td><dl class="dl-horizontal">
                    <dt>
                        <label>联系方式</label>
                    </dt>
                    <dd>
                        <div class="input-append">
                            <input class="span2" type="text" placeholder="联系方式">
                        </div>
                    </dd>
                </dl></td>
        </tr>
        <tr>
            <td colspan="2"><dl class="dl-horizontal">
                    <dt>
                        <label>备注</label>
                    </dt>
                    <dd>
                        <div class="input-prepend">
                            <input class="input-xxlarge" type="text" placeholder="500字以内">
                        </div>
                    </dd>
                </dl></td>
        </tr>
    </table>
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active">
                <a href="#tab1" data-toggle="tab">发货地址</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="100">姓名</th>
                                    <th width="100">手机号码</th>
                                    <th width="100">座机号码</th>
                                    <th width="100">国家</th>
                                    <th width="100">省</th>
                                    <th width="100">市</th>
                                    <th width="300">地址</th>
                                    <th width="100">邮编</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php for ($i=0; $i<5; $i++):?>
                                <tr>
                                    <td><input type="checkbox" value="29" name="id[]"></td>
                                    <td>小哈哈</td>
                                    <td>13410260771</td>
                                    <td>0719-8610550</td>
                                    <td>中国</td>
                                    <td>湖北省</td>
                                    <td>十堰市</td>
                                    <td>测试用主题</td>
                                    <td>420000</td>
                                </tr>
                                <?php endfor;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:500px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">新增货币</h3>
    </div>
    <div class="modal-body">
        <form name="dialog_form" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="name">货币名</label>
                <div class="controls">
                    <input type="text" id="name" placeholder="货币名">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="code">代码</label>
                <div class="controls">
                    <input type="text" id="code" placeholder="英文代码">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary">确定</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	var dialog = $('#dialog').modal('hide');
	
	var slave = new lyq.ListEx($('#list1'), {
		full_size:true,
		events: {
			dblclick:function (evn) {
				alert(this);
			}
		}	
	});
});
</script>
<?=JsUtils::ob_end();?>