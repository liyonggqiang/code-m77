<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">设置产地</a>
        <ul class="nav nav-pills">
            <li><a href="<?=$this->buildUrl('ajaxadd')?>" id="btn_add">增加</a>
            <li><a href="<?=$this->buildUrl('ajaxdelete')?>" id="btn_nav_delete">删除</a></li>
        </ul>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="300">产地名称</th>
                        <th>代码</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr>
                            <td><input type="checkbox" d='{"id":"1","country":"法国","code":"FR"}'></td>
                            <td>法国</td>
                            <td>FR</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="pull-left"> 共 8 条记录 </div>
            <div class="pagination pagination-right pagination-small">
                <ul>
                    <li><a href="#">&lt;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="hide" id="dia">
    <form name="dialog_form" class="form-horizontal">
    	<input type="hidden" name="country_id" />
        <div class="control-group">
            <label class="control-label" for="country">产地名</label>
            <div class="controls">
                <input type="text" id="country" placeholder="产地名">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="code">代码</label>
            <div class="controls">
                <input type="text" id="code" placeholder="英文代码">
            </div>
        </div>
    </form>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
	var form = document.forms['dialog_form'];
	
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				var obj = list.dataForSelectedRow();
				form['country_id'].value = obj.id;
				form['country'].value = obj.country;
				form['code'].value = obj.code;
				
				bootbox.dialog({
					message: $('#dia'),
					title: "修改产地",
					buttons: {
						cancel: {
							label: "取消"
						},
						success: {
							label: "确定",
							className: "btn-primary",
							callback: function() {
								
							}
						}
					}
				});
            }
        }
    });
	
	
	$('#btn_add').click(function (evn) {
		evn.preventDefault();
		form.reset();
		
		bootbox.dialog({
			message: $('#dia'),
			title: "新增产地",
			buttons: {
				cancel: {
					label: "取消"
				},
				success: {
					label: "确定",
					className: "btn-primary",
					callback: function() {
						
					}
				}
			}
		});
	});
    
    $('#btn_nav_delete').click(function (evn) {
        evn.preventDefault();
        lyq.AjaxUtlis.submitAndRefresh(this.href, {id:list.dataForSelectedRow().id});
    });
});
</script>
<?=JsUtils::ob_end();?>