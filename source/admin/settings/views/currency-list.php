<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">设置货币汇率</a>
        <ul class="nav nav-pills">
            <li><a href="<?=$this->buildUrl('add')?>">增加</a>
            <li><a href="<?=$this->buildUrl('ajaxdelete')?>" id="btn_nav_delete">删除</a></li>
        </ul>
    </div>
</div>

<div class="fix-box">
    <div class="table-list" id="list">
        <div class="header">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th width="20"><input type="checkbox"></th>
                        <th width="200">货币名称</th>
                        <th width="200">代码</th>
                        <th>对人民币汇率</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="body">
            <table id="st" class="table table-striped table-hover table-condensed table-bordered">
                <tbody>
                    <?php for ($i = 0; $i < 50; $i++): ?>
                        <tr link="<?=$this->buildUrl('edit',null,null,array('id'=>1))?>">
                            <td><input type="checkbox"></td>
                            <td>港币</td>
                            <td>HKD</td>
                            <td>0.8</td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function() {
    var list = new lyq.ListEx($('#list'), {
        events: {
            dblclick: function(evn) {
				window.location = $(this).attr('link');
            }
        }
    });
    
    $('#btn_nav_delete').click(function (evn) {
        evn.preventDefault();
        lyq.AjaxUtlis.submitAndRefresh(this.href, {id:list.dataForSelectedRow().id});
    });
});
</script>
<?=JsUtils::ob_end();?>