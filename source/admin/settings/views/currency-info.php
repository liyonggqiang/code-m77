<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">货币&amp;汇率</a>
        <ul class="nav nav-pills">
            <li>
                <a href="#">保存</a>
            </li>
            <li>
                <a href="#">删除</a>
            </li>
        </ul>
    </div>
</div>

<form name="frm_info" method="post">
<div class="padd fix-box">
    <table class="table table-condensed table-noborder">
        <tr>
            <td width="400"><dl class="dl-horizontal">
                    <dt>
                        <label>货币名</label>
                    </dt>
                    <dd>
                        <div>
                            <input class="span2" type="text" placeholder="货币名">
                        </div>
                    </dd>
                </dl></td>
            <td><dl class="dl-horizontal">
                    <dt>
                        <label>代码</label>
                    </dt>
                    <dd>
                        <div>
                            <input class="span2" type="text" placeholder="英文代码">
                        </div>
                    </dd>
                </dl></td>
        </tr>
    </table>
    <div class="tabbable" id="tabs1">
        <ul class="nav nav-tabs" style="margin-bottom:10px;">
            <li class="active">
                <a href="#tab1" data-toggle="tab">汇率</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="table-list" id="list1">
                    <div class="header">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th width="20"><input type="checkbox" id="chk_all"></th>
                                    <th width="300" slv_field="date">日期</th>
                                    <th slv_field="rate">汇率</th>
                                    <th slv_field="id">ID</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="body">
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php for ($i=0; $i<10; $i++):?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>2011-01-26</td>
                                    <td>0.796</td>
                                    <td>99</td>
                                </tr>
                                <?php endfor;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<div class="hide" id="dia">
    <form name="dialog_form" class="form-horizontal">
        <input type="hidden" name="id" />
        <div class="control-group">
            <label class="control-label" for="name">日期</label>
            <div class="controls">
                <div class="input-append" id="dp1">
                    <input class="input-medium" type="text" name="date" placeholder="日期">
                    <span class="btn add-on">
                      <i class="fa fa-calendar" data-date-icon="fa fa-calendar" data-time-icon="fa fa-time"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="rate">当天汇率</label>
            <div class="controls">
                <input type="number" id="rate" name="rate" min="0.1" max="99.9" step="0.1" value="1.0" placeholder="当天汇率">
            </div>
        </div>
    </form>
</div>
<?=JsUtils::ob_start();?>
<script>
$(function () {
	lyq.TabEx.fullsizeTab($('#tabs1'));
	
	$('#dp1').datetimepicker({
		pickTime: false
    });
	
	var form = document.forms['dialog_form'];
	$(form).bind('submit', function (evn) {
		evn.preventDefault();
		
	});
	
	var list = new lyq.ListEx($('#list1'), {
		full_size:true,
		slave_table: {
			post_name:'std',
			hide_fields:['id'],
		},
		contextmenu:{
			'新增汇率':function () {
				form.reset();
				bootbox.dialog({
					message: $('#dia'),
					title: "新增汇率",
					buttons: {
						cancel: {
							label: "取消"
						},
						success: {
							label: "确定",
							className: "btn-success",
							callback: function() {
								list.slaveTable.addRow({
									'date':form['date'].value,
									'rate':form['rate'].value
								});
							}
						}
					}
				});
			},
			'修改选择':function () {
				var idx = list.slaveTable.selectedRowIndex();
				if (-1 == idx)
				{
					bootbox.alert("请选择需要编辑的行。");
					return;	
				}
				
				var data = list.slaveTable.dataForSelectedRow();
				
				form['id'].value = data.id.data;
				form['date'].value = data.date.data;
				form['rate'].value = data.rate.data;
				
				bootbox.dialog({
					message: $('#dia'),
					title: "修改汇率",
					buttons: {
						cancel: {
							label: "取消"
						},
						success: {
							label: "确定",
							className: "btn-success",
							callback: function() {
								list.slaveTable.updateRow(list.slaveTable.selectedRowIndex(), {
									'id':form['id'].value,
									'date':form['date'].value,
									'rate':form['rate'].value
								});
							}
						}
					}
				});
			},
			'删除选中':function () {
				list.slaveTable.removeRow(list.slaveTable.selectedRowIndex());
			}
		}
	});
});
</script>
<?=JsUtils::ob_end();?>