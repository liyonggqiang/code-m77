<?php

/**
 * Description of CountryController
 *
 * @author LYQ
 */
class Settings_CountryController extends BaseController
{
    public function listAction ()
    {
        $this->render('country-list');
    }
	
	public function ajaxaddAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}
	
	public function ajaxupdateAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}
	
	public function ajaxdeleteAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}
    
    public function popuplistAction ()
    {
        $this->render('popup-country-list');
    }
}