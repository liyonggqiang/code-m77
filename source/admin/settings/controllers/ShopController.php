<?php

/**
 * Description of ShopController
 *
 * @author LYQ <coderfly@163.com>
 */
class Settings_ShopController extends BaseController
{
    public function listAction ()
    {
        $this->render('shop-list');
    }

    public function ajaxaddAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}

	public function ajaxupdateAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}

	public function ajaxdeleteAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}
}
