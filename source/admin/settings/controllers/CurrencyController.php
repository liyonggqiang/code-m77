<?php

/**
 * Description of CurrencyController
 *
 * @author LYQ
 */
class Settings_CurrencyController extends BaseController
{
    public function listAction ()
    {
        $this->render('currency-list');
    }
	
	public function addAction ()
	{
		$this->render('currency-info');
	}
	
	public function editAction ()
	{
		$this->render('currency-info');
	}
	
	public function ajaxdeleteAction ()
	{
		AjaxUtils::json(__FUNCTION__);
	}
    
    public function popuplistAction ()
    {
        $this->render('popup-currency-list');
    }
}