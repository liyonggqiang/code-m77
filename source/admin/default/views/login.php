<style>
fieldset {
	width:500px;
	margin:auto;
	margin-top:100px;
	padding:10px;	
}

label {
	float:left;
	width:100px;	
}

p {
	margin:3px;
}
</style>
<fieldset>
    <legend><strong>Code:m77</strong></legend>
    <form action="<?=$this->buildUrl('auth') ?>" method="post" style="padding:0 35px;">
        <p><input type="text" name="uname" placeholder="用户名"></p>
        <p><input type="password" name="upwd" placeholder="密码"></p>
        <p><input class="btn btn-primary" type="submit" value="&nbsp;&nbsp;登&nbsp;录&nbsp;&nbsp;"></p>
    </form>
</fieldset>
<?=JsUtils::ob_start();?>
<script type="text/javascript">
$(function ()
{
	if (window != top)
	{
		top.location.replace('<?=$this->buildUrl('login')?>');
	}
});
</script>
<?=JsUtils::ob_end();?>