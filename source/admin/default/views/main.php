<header>
    <div role="banner" class="navbar navbar-fixed-top bs-docs-nav">
        <div class="container" style="width:100%"> 
            <!-- Menu button for smallar screens -->
            <div class="navbar-header">
                <button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle btn-navbar"><span>Menu</span></button>
                <a class="pull-left menubutton hidden-xs" href="#"><i class="fa fa-bars"></i></a> 
                <!-- Site name for smallar screens --> 
                <a class="navbar-brand" href="index.html">Code:<span class="bold">m77</span></a> </div>
            
            <!-- Navigation starts -->
            <nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse"> 
                <!-- Links --> 
                
                <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right user-data">            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src=""> John <span class="bold">Doe</span> <b class="caret"></b>              
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
              <li><a href="#"><i class="fa fa-cogs"></i> Settings</a></li>
              <li><a href="login.html"><i class="fa fa-key"></i> Logout</a></li>
            </ul>
          </li>
          <!-- Upload to server link. Class "dropdown-big" creates big dropdown -->
          <li class="dropdown dropdown-big leftonmobile">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-cloud-upload"></i></a>
            <!-- Dropdown -->
            <ul class="dropdown-menu">
              <li>
                <!-- Progress bar -->
                <p>Photo Upload in Progress</p>
                <!-- Bootstrap progress bar -->
                <div class="progress progress-striped active">
					<div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-info">
						<span class="sr-only">40% Complete</span>
					</div>
			    </div>

                <hr>

                <!-- Progress bar -->
                <p>Video Upload in Progress</p>
                <!-- Bootstrap progress bar -->
                <div class="progress progress-striped active">
					<div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-success">
						<span class="sr-only">80% Complete</span>
					</div>
			    </div> 

                <hr>             

                <!-- Dropdown menu footer -->
                <div class="drop-foot">
                  <a href="#">View All</a>
                </div>

              </li>
            </ul>
          </li>

          <!-- Sync to server link -->
          <li class="dropdown dropdown-big leftonmobile">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-refresh"></i></a>
            <!-- Dropdown -->
            <ul class="dropdown-menu">
              <li>
                <!-- Using "fa fa-spin" class to rotate icon. -->
                <p><span class="label label-info"><i class="fa fa-cloud"></i></span> Syncing Photos to Server</p>
                <hr>
                <p><span class="label label-warning"><i class="fa fa-cloud"></i></span> Syncing Bookmarks Lists to Cloud</p>

                <hr>

                <!-- Dropdown menu footer -->
                <div class="drop-foot">
                  <a href="#">View All</a>
                </div>

              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-big leftonmobile">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <i class="fa fa-comments"></i><span class="label label-info">6</span> 
              </a>

                <ul class="dropdown-menu">
                  <li class="dropdown-header padless">
                    <!-- Heading - h5 -->
                    <h5><i class="fa fa-comments"></i> Chats</h5>
                    <!-- Use hr tag to add border -->                   
                  </li>
                  <li>
                    <hr>
                    <!-- List item heading h6 -->
                    <h6><a href="#">Hi :)</a> <span class="label label-warning pull-right">10:42</span></h6>
                    <div class="clearfix"></div>
                    <hr>
                  </li>
                  <li>
                    <h6><a href="#">How are you?</a> <span class="label label-warning pull-right">20:42</span></h6>
                    <div class="clearfix"></div>
                    <hr>
                  </li>
                  <li>
                    <h6><a href="#">What are you doing?</a> <span class="label label-warning pull-right">14:42</span></h6>
                    <div class="clearfix"></div>
                    <hr>
                  </li>                  
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li>
            
            <!-- Message button with number of latest messages count-->
            <li class="dropdown dropdown-big messages-dd leftonmobile">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <i class="fa fa-envelope-o"></i> <span class="label label-primary">3</span> 
              </a>

                <ul class="dropdown-menu">
                  <li class="dropdown-header padless">
                    <!-- Heading - h5 -->
                    <h5><i class="fa fa-envelope-alt"></i> Messages</h5>
                    <!-- Use hr tag to add border -->
                    
                  </li>
                  <li>
                    <hr><!-- List item heading h6 -->
                    <h6><a href="#">Hello how are you?</a></h6>
                    <!-- List item para -->
                    <p>Quisque eu consectetur erat eget  semper...</p>
                    <hr>
                  </li>
                  <li>
                    <h6><a href="#">Today is wonderful?</a></h6>
                    <p>Quisque eu consectetur erat eget  semper...</p>
                    <hr>
                  </li>
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li>

            <!-- Members button with number of latest members count -->
            <li class="dropdown dropdown-big">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <i class="fa fa-user"></i> <span class="label label-success">7</span> 
              </a>

                <ul class="dropdown-menu">
                  <li class="dropdown-header padless">
                    <!-- Heading - h5 -->
                    <h5><i class="fa fa-user"></i> Users</h5>
                    <!-- Use hr tag to add border -->                    
                  </li>
                  <li>
                    <hr>
                    <!-- List item heading h6-->
                    <h6><a href="#">John Doe</a> <span class="label label-warning pull-right">Free</span></h6>
                    <div class="clearfix"></div>
                    <hr>
                  </li>
                  <li>
                    <h6><a href="#">Iron Man</a> <span class="label label-important pull-right">Premium</span></h6>
                    <div class="clearfix"></div>
                    <hr>
                  </li>
                  <li>
                    <h6><a href="#">Salamander</a> <span class="label label-warning pull-right">Free</span></h6>
                    <div class="clearfix"></div>
                    <hr>
                  </li>                  
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li> 
        </ul>
                
            </nav>
        </div>
    </div>
</header>
<div class="content"> 
	<div class="sidebar">
        <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
        <ul id="nav" style="">
          <li><a href="index.html"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
          <li><a href="<?=$this->buildUrl('list','customer','crm')?>"><i class="fa fa-group"></i> <span>客户管理</span></a></li>
          <li class="has_sub"><a href="#" class=""><i class="fa fa-list-alt"></i> <span>商品管理</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
            <ul style="display: none;">
              <li><a href="<?=$this->buildUrl('list','item','products')?>">产品列表</a></li>
              <li><a href="<?=$this->buildUrl('list','brand','products')?>">品牌设置</a></li>
              <li><a href="<?=$this->buildUrl('list','category','products')?>">类别设置</a></li>
              <li><a href="<?=$this->buildUrl('list','product','products')?>">商品系列</a></li>
            </ul>
          </li> 
          <li><a href="<?=$this->buildUrl('list','salesorder','sales')?>"><i class="fa fa-bar-chart-o"></i> <span>销售管理</span></a></li>
          <li><a href="<?=$this->buildUrl('list','deliveryorder','delivery')?>"><i class="fa fa-table"></i> <span>出货管理</span></a></li>
          <li><a href="<?=$this->buildUrl('list','parcelorder','parcel')?>"><i class="fa fa-table"></i> <span>发货管理</span></a></li>
          <li><a href="<?=$this->buildUrl('list','purchaseorder','sales')?>"><i class="fa fa-table"></i> <span>采购管理</span></a></li>
          <li><a href="<?=$this->buildUrl('list','receivingorder','sales')?>"><i class="fa fa-table"></i> <span>收货管理</span></a></li>
          <li class="has_sub"><a href="#" class=""><i class="fa fa-th"></i> <span>仓库管理</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
            <ul style="display: none;">
              <li><a href="<?=$this->buildUrl('list','stock','stock')?>">库存清单</a></li>
              <li><a href="<?=$this->buildUrl('slave','test','stock')?>">盘点</a></li>
            </ul>
          </li> 
          <li class="has_sub"><a href="#" class=""><i class="fa fa-wrench"></i> <span>系统设置</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
            <ul style="display: none;">
              <li><a href="<?=$this->buildUrl('list','country','settings')?>">生产国家</a></li>
              <li><a href="<?=$this->buildUrl('list','currency','settings')?>">币种汇率</a></li>
              <li><a href="<?=$this->buildUrl('list','shop','settings')?>">贩卖店铺</a></li>
            </ul>
          </li>
          <li class="has_sub"><a href="#" class=""><i class="fa fa-list-alt"></i> <span>Test</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
            <ul style="display: none;">
              <li><a href="<?=$this->buildUrl('list','test','test')?>">list</a></li>
              <li><a href="<?=$this->buildUrl('slave','test','test')?>">slave</a></li>
              <li><a href="<?=$this->buildUrl('tabs','test','test')?>">tabs</a></li>
              <li><a href="<?=$this->buildUrl('popupwin','test','test')?>">popupwin</a></li>
              <li><a href="<?=$this->buildUrl('dialog','test','test')?>">dialog</a></li>
            </ul>
          </li> 
        </ul>
    </div>
	<div class="mainbar" style="min-height:inherit;">
    	<iframe id="main_frame" frameborder="0" width="100%" height="100%" src="<?=$this->buildUrl('welcome')?>" scrolling="auto"></iframe>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
$(top).bind('set_frame', function (evn, url)
{
	document.getElementById('main_frame').src = url;
});
</script>