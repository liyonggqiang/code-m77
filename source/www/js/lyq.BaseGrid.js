/**
 * BaseGrid
 * 
 * @author Steven
 */

var lyq = lyq || {};

lyq.Table = function (table, configs)
{
	table = $(table);
	
	this.configs = $.extend({
		enabled : true,
		post_name : 'bg'
	}, configs);
	
	this.table = table;
	this.header = table.find('thead');
	this.body = table.find('tbody');
	this.foot = table.find('tfoot');

	var self = this;
	
    //set columes
    //edit by MoLice<sf.molice@gmail.com> 2012-07-05
    /**
    * 初始化<col />标签，若已存在则引用，不存在则生成
    * @param {Integer} len col的数量
    */
    function initCol(len) 
    {
        var col_exist = table.find("col");
        if(col_exist.size() > 0) {
            if(col_exist.size() != len) {
                // col数量与实际列数不一致，将其复制为一致，以便处理
                col_exist.each(function() {
                    // TODO 处理若干个colgroup标签包裹col标签的情况
                    if($(this).attr("span")) {
                        var span = parseInt($(this).attr("span"));
                        for(var i=0; i<span; i++) {
                            $(this).clone().removeAttr("span").insertAfter($(this));
                        }
                        $(this).remove();
                    }
                });
            }
        }
        else
        {
            var colgroup = ("<colgroup/>").prependTo(table);
            for (var i=0; i<len; i++) {
                $("<col />").appendTo(colgroup);
            }
        }
        return table.find("col");
    }

    var columes_width = 0;
    if (this.header.length)
    {
        // 如果表格中有定义thead，则按照th数量决定col数量
        var header_cols = this.header[0].rows[0].cells;
        initCol(header_cols).each(function(index) {
            columes_width += $(this).attr("_field_", header_cols[index].getAttribute("_field_")).width();
        });
    } else if (this.configs.columes) {
        // 如果配置中有给出columes，则根据columes长度决定col数量
        initCol(this.configs.columes.length).each(function(index) {
            columes_width += $(this).attr('_field_', self.configs.columes[index]).width();
        });
    }
    else if (this.configs.header)
    {
        var col;
        var header_cols = this.configs.header.cells;
        initCol(header_cols).each(function(index) {
            columes_width += $(this).attr('_field_', header_cols[index].getAttribute('_field_') ? header_cols[index].getAttribute('_field_') : index)
            .width(header_cols[index].width ? header_cols[index].width : header_cols[index].offsetWidth).width();
        });
        table.width(this.configs.header.offsetParent.width ? this.configs.header.offsetParent.width : this.configs.header.offsetParent.offsetWidth);
    }
	
		//set events
	if (self.configs.events)
	{
		for (var e in self.configs.events)
		{
			this.body.find('tr').live(e, self, self.configs.events[e]);
		}
	}
	
	this.refresh_cells();
	
	delete this.configs.enabled;
	this.set_enabled(configs.enabled);
}

lyq.Table.prototype.refresh_cells = function ()
{
	var rows = this.body[0].rows;
	var cells;
	var cols = this.table[0].getElementsByTagName('col');
	
	for (var i=0; i<rows.length; i++)
	{
		cells = rows[i].cells;
		
		for (var j=0; j<cells.length; j++)
		{
			this.render_cell(cells[j], cols);
		}
	}
}

lyq.Table.prototype.render_cell = function (cell, cols)
{
	if (cols[cell.cellIndex])
	{
		cell.setAttribute('_field_', cols[cell.cellIndex].getAttribute('_field_'));
	}
	else
	{
		cell.setAttribute('_field_', cell.cellIndex);
	}
	
	var div = $('<div class="cell">');
	
	var jcell = $(cell);
	
	if (this.configs.editable)
	{
		var span = $('span');
		var flag = true;
		
		for (var i=0; i<cell.childNodes.length; i++)
		{
			if (1 == cell.childNodes[i].nodeType)
			{
				flag = false;
				break;
			}
		}
		
		if (1 == span.length || flag)
		{
			var json;
			var label = $('<label>');
			var input = $('<input type="hidden">');
			input.attr('name', this.configs.post_name + '[' + cell.parentNode.rowIndex + '][' + jcell.attr('_field_') + ']');
			
			try
			{
				json = span.length ? $.parseJSON(span.html()) : $.parseJSON(cell.innerHTML);
			}
			catch (e)
			{}
			
			if (json && 'object' == typeof json)
			{
				label.html(json.label);
				input.val(json.data ? json.data : json.label);
			}
			else
			{
				label.html(cell.innerHTML);
				input.val(cell.innerHTML);
			}
			
			div.append(label);
			div.append(input);
		}
		else
		{
			div.append(cell.childNodes);
		}
	}
	else
	{
		div.append(cell.childNodes);
	}
	
	jcell.empty();
	jcell.append(div);
	
	this.redraw();
}

lyq.Table.prototype.redraw = function ()
{
	this.body.find('tr:odd').addClass('odd');
}

lyq.Table.prototype.set_enabled = function (enabled)
{
	enabled = true && enabled;
	
	if (this.configs.enabled == enabled)
	{
		return;
	}
	
	if (enabled)
	{
		this.table.find('._table_cover').remove();
	}
	else
	{
		var cover = document.createElement('div');
		cover.setAttribute('class', '_table_cover');
		
		with (cover.style)
		{
			backgroundColor = '#eee';
			position = 'absolute';
			opacity = '.3';
			top = '0px';
			left = '0px';
			bottom = '0px';
			right = '0px';
			zIndex = '999';
			width = this.table[0].width + 'px';
		}
		
		this.table.before(cover);
	}
	
	this.configs.enabled = enabled;
}

lyq.Table.prototype.add_row = function (items)
{
	var cols = this.table[0].getElementsByTagName('col');
	var rows_length = this.body[0].rows.length;
	var cells_length = this.header[0].rows[0].cells.length;
	var row =  this.body[0].insertRow(rows_length);
	var cell = null;
	
	for (var i=0; i<cells_length; i++)
	{
		cell = row.insertCell(i);
		cell.innerHTML = items[i];
		
		this.render_cell(cell, cols);
	}
}

lyq.Table.prototype.get_row = function (index)
{
	var row = this.body[0].rows[index];
	if (!row)
	{
		return null;
	}
	
	var ret_obj = {};
	
	var f;
	$(row.cells).each(function ()
	{
		f = this.getAttribute('_field_');
		
		if (!f || '' == f)
		{
			return;
		}
		
		ret_obj[f] = $(this).find('input').val();
	});
	
	return ret_obj;
}

lyq.BaseGrid = function (table, configs)
{
	table = $(table);
	this.configs = {
		allow_multiple_selection : false
	};
	
	if (configs)
	{
		for (var p in configs)
		{
			this.configs[p] = configs[p];
		}
	}
	
	var self = this;
	
	this.selected_index = -1;
	this.selected_row = undefined;
	
	this.selected_rows = [];
	
	var header_div = table.find('div.thead');
	var body_div = table.find('div.tbody');
	
	this.header = header_div.find('table:first');
	this.body = body_div.find('table:first');
	this.footer = table.find('div.tfoot');
	
	if (this.header.length)
	{
		var header_cols = this.header[0].rows[0].cells;
		var columes_width = 0;
		for (var i=0; i<header_cols.length; i++)
		{
			columes_width += parseInt(header_cols[i].width ? header_cols[i].width : header_cols[i].offsetWidth);
		}
		
		this.header[0].width = columes_width;
		
			//为子TABLE配置HEADER
		this.configs.header = this.header[0].rows[0];
	}
	
	table.find('div.tbody').scroll(function ()
	{
		header_div.scrollLeft(body_div.scrollLeft());
	});
	
	this.tbody = new lyq.Table(this.body, this.configs);
	
	this.tbody.body.find('tr').live('click', function ()
	{
		var flag = false;
		var selected_rows = [];
		for (var i=0; i<self.selected_rows.length; i++)
		{
			if (this == self.selected_rows[i])
			{
				flag = true;
				continue;
			}
			
			selected_rows.push(self.selected_rows[i]);
		}
		
		if (flag)
		{
			self.selected_index = -1;
			self.selected_row = undefined;
			delete self.selected_rows;
			self.selected_rows = selected_rows;
			
			return;
		}
		
		self.selected_index = this.rowIndex;
		self.selected_rows.push(this);
		self.selected_row = this;
	}).live('mouseover', function ()
	{
		this.style.backgroundColor = '#ffffcc';
	}).live('mouseout', function ()
	{
		this.style.backgroundColor = '';
	});
	
	this.selected_indies = [];
	this.selected_rows = [];
}

lyq.BaseGrid.prototype.init = function ()
{
	
}

lyq.BaseGrid.prototype.selecte_all = function ()
{
	if (!this.configs.allow_multiple_selection)
		return;
	
	$(this).troggle('event_selected_all');
}

lyq.BaseGrid.prototype.clear_selection = function ()
{
	this.selected_index = -1;
	
	var rows = this.table[0].rows;
	for (var i=0; i<rows; i++)
	{
		rows[i].style.backgroundColor = '';
		rows[i].setAttribute('_sel_', null);
	}
}
