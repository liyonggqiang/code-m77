/**
 * SlaveTable grid
 * new SlaveTable
 *
 * @author LYQ
 */
var SlaveTable = function(table, options)
{
	if (!table)
		return;
		
	this.options = options || {};
	
	this.table = table;
	this.row_increase_no = 0;
	this.disabled = false;
	
	//this.egt_table = new Table(table, options);
	
	this.post_name = options && options.post_name ? options.post_name : 'dg_' + SlaveTable.instance_no;
		
	var trash_table = document.createElement('table');
	trash_table.style.display = 'none';
	this.trash_table = $(trash_table);
	
	this.table.parent().append(this.trash_table);
		
	this.init();
	
	if (options.contextmenu)
		this.contextmenu_box = this.creat_contextmenu(options);
};

SlaveTable.instance_no = 0;

SlaveTable.prototype.init = function ()
{
	this.row_increase_no = 0;
	
	var self = this;
	
	this.table.find('tr').each(function (i)
	{
		//$(this).find('td:not(:has(>input:checkbox))').each(function (j)
		$(this).find('td').each(function (j)
		{
			if (!self.options.fields || null === self.options.fields[j])
			{
				this.setAttribute('_field_', j);
				return;
			}
			
			this.setAttribute('_field_', self.options.fields[j]);
			
			var content = {};
			
			try
			{
				content = jQuery.parseJSON($(this).text());
			}
			catch (e)
			{}
			
			if (!isNaN(content))
					content = {};
			
			var span = document.createElement('span');
			var label = document.createElement('label');
			label.innerHTML = content.label ? content.label : $(this).html();
			
			var input = document.createElement('input');
			input.type = 'hidden';
			input.value = content.data ? content.data : $(this).text();
			
			if (self.options.fields && self.options.fields[j])
				input.name = self.post_name + '[' + i + '][' + self.options.fields[j] + ']';
			else
				input.name = self.post_name + '[' + i + '][]';
			
			span.appendChild(label);
			span.appendChild(input);
			
			this.innerHTML = '';
			this.appendChild(span);
			
			//this.setAttribute('_field_', (self.options.fields && self.options.fields[j]) ? self.options.fields[j] : j);	
		});
		
			//make a row flag
		var flag_inp = document.createElement('input');
		flag_inp.type = 'hidden';
		flag_inp.name = self.post_name + '[' + i + '][_flag_]';
		flag_inp.value = '_og_';
		this.cells[0].appendChild(flag_inp);
		
		self.row_increase_no = i + 1;
	});
	
	//this.egt_table.redraw();
}

SlaveTable.prototype.selected_index = function ()
{
	return this.egt_table.selected_index;
}

SlaveTable.prototype.hide_fields = function (fields)
{
	if (!$.isArray(fields))
		return;
	
	$(this.options.header.offsetParent).find('col').each(function (i)
	{
		if (-1 != $.inArray(this.getAttribute('_field_'), fields))
			this.style.visibility = 'collapse';
	});
}

/* mothed for items */
SlaveTable.prototype.remove_row = function ()
{
	var chk_boxs = $(this.table).find('td>input:checkbox');
	
	var row;
	var cell;
	for (var i=chk_boxs.length-1; i>=0; i--)
	{
		if (chk_boxs[i].checked)
		{
			cell = chk_boxs[i].parentNode;
			row = chk_boxs[i].parentNode.parentNode;
			
			if ($(cell).find('input[value=_og_]').length)
			{
				$(cell).find('input[value=_og_]').val('_delete_');
				this.trash_table.appendChild(row);
			}
			else if ($(cell).find('input[value=_update_]').length)
			{
				$(cell).find('input[value=_update_]').val('_delete_');
				this.trash_table.appendChild(row);
			}			
			
			if ($(cell).find('input[value=_new_]').length)
			{
				if (this.options.no_header)
					this.table.deleteRow(i);
				else
					this.table.deleteRow(i+1)
			}
		}
	}
	
	this.egt_table.redraw();
};

SlaveTable.prototype.remove_all_rows = function ()
{
	var chk_boxs = $(this.table).find('td>input:checkbox');
	
	var row;
	var cell;
	for (var i=chk_boxs.length-1; i>=0; i--)
	{
		cell = chk_boxs[i].parentNode;
		row = chk_boxs[i].parentNode.parentNode;
		
		if ($(cell).find('input[value=_og_]').length)
		{
			$(cell).find('input[value=_og_]').val('_delete_');
			this.trash_table.appendChild(row);
		}
		else if ($(cell).find('input[value=_update_]').length)
		{
			$(cell).find('input[value=_update_]').val('_delete_');
			this.trash_table.appendChild(row);
		}			
		
		if ($(cell).find('input[value=_new_]').length)
		{
			if (this.options.no_header)
				this.table.deleteRow(i);
			else
				this.table.deleteRow(i+1)
		}
	}
}

SlaveTable.prototype.insert_row = function (data)
{
	if (!$.isArray(data))
		return;
	
	var self = this;
	var row = this.table.insertRow(this.table.rows.length);
	
	for (var i=0; i<data.length; i++)
	{
		var cell = row.insertCell(i);
		
		cell.setAttribute('width', this.options.header.cells[i].getAttribute('width'));
		cell.setAttribute('_field_', self.options.fields[i]);
		
		if (0 == i)
		{
			var flag_inp = document.createElement('input');
			flag_inp.type = 'hidden';
			flag_inp.name = this.post_name + '[' + this.row_increase_no + '][_flag_]';
			flag_inp.value = '_new_';
						
			cell.appendChild(flag_inp);
			
			if (null === this.options.fields[0])
			{
				var chkbox = document.createElement('input');
				chkbox.type = 'checkbox';
				cell.appendChild(chkbox);
				
				continue;
			}
		}

		if (null === data[i])
			continue;
		
		var span = document.createElement('span');
		var label = document.createElement('label');
		if (data[i].label)
			label.innerHTML = data[i].label;
		else if(data[i].label == "")
			label.innerHTML = data[i].data;
		else
			label.innerHTML = data[i];
		
		var input = document.createElement('input');
		input.type = 'hidden';
		
		if (!$.isArray(this.options.fields) || null === this.options.fields[i])
			input.name = this.post_name + '[' + this.row_increase_no + '][' + (i+1) + ']';
		else
			input.name = this.post_name + '[' + this.row_increase_no + '][' + this.options.fields[i] + ']';	
		
		if (data[i].data || data[i].data == "")
			input.value = data[i].data;
		else
			input.value = data[i];
		
		if (false === data[i].visible)
			cell.style.display = 'none';
		
		span.appendChild(label);
		span.appendChild(input);
		
		cell.appendChild(span);
	}
	
	this.row_increase_no++;
	
	this.egt_table.redraw();
}

SlaveTable.prototype.insert_row_assoc = function (data)
{
	if ('object' != typeof data)
		return;

	var self = this;
	var row = this.table.insertRow(this.table.rows.length);

	for (var i=0,p=null; i<self.options.fields.length; i++)
	{
		var d = data[self.options.fields[i]];
		var cell = row.insertCell(i);

		if (!d)
			continue;
		
		cell.setAttribute('width', this.options.header.cells[i].getAttribute('width'));
		cell.setAttribute('_field_', self.options.fields[i]);

		if (0 == i)
		{
			var flag_inp = document.createElement('input');
			flag_inp.type = 'hidden';
			flag_inp.name = this.post_name + '[' + this.row_increase_no + '][_flag_]';
			flag_inp.value = '_new_';

			cell.appendChild(flag_inp);

			if (null === this.options.fields[0])
			{
				var chkbox = document.createElement('input');
				chkbox.type = 'checkbox';
				cell.appendChild(chkbox);

				continue;
			}
		}

		if (null === data[p])
			continue;

		var span = document.createElement('span');
		var label = document.createElement('label');
		if (d.label)
			label.innerHTML = d.label;
		else if(d.label == "")
			label.innerHTML = d.data;
		else
			label.innerHTML = d;

		var input = document.createElement('input');
		input.type = 'hidden';

		if (!$.isArray(this.options.fields) || null === this.options.fields[i])
			input.name = this.post_name + '[' + this.row_increase_no + '][' + (i+1) + ']';
		else
			input.name = this.post_name + '[' + this.row_increase_no + '][' + this.options.fields[i] + ']';

		if (d.data || d.data == "")
			input.value = d.data;
		else
			input.value = d;

		if (false === d.visible)
			cell.style.display = 'none';

		span.appendChild(label);
		span.appendChild(input);

		cell.appendChild(span);
	}

	this.row_increase_no++;

	this.egt_table.redraw();
}

SlaveTable.prototype.update_row = function (index, data)
{
	if (!$.isArray(data) ||
		0 > index ||
		index >= this.table.rows.length)
		return;
	
	var row;
	
	index = isNaN(index) ? this.egt_table.selected_index : index;
	row = row = this.egt_table.rows[index];
	
	var cell = $(row.cells[0]);
	
	if (cell.find('input[value=_og_]').length)
		cell.find('input[value=_og_]').val('_update_');
	
	for (var i=0,j=i+1; i<data.length; i++,j++)
	{
		if (null == data[i])
			continue;
		
		cell = $(row.cells[j]);
		
		if (data[i].label)
			cell.find('span>label').text(data[i].label);
		else if(data[i].label == "")
			cell.find('span>label').text(data[i].data);
		else
			cell.find('span>label').text(data[i]);
			
		if (data[i].data || data[i].data == "")
			cell.find('span>input').val(data[i].data);
		else
			cell.find('span>input').val(data[i]);
		
		if (false === data[i].visible)
			cell.style.display = 'none';
	}
}

SlaveTable.prototype.update_row_assoc = function (index, data)
{
	if (!$.isArray(data) ||
		0 > index ||
		index >= this.table.rows.length)
		return;

	var row;

	index = isNaN(index) ? this.egt_table.selected_index : index;
	row = row = this.egt_table.rows[index];

	if (cell.find('input[value=_og_]').length)
		cell.find('input[value=_og_]').val('_update_');

	for (var p in data)
	{
		if (null == data[p])
			continue;

		cell = $(row).find('_field_')

		if (data[p].label)
			cell.find('span>label').text(data[p].label);
		else if(data[p].label == "")
			cell.find('span>label').text(data[p].data);
		else
			cell.find('span>label').text(data[p]);

		if (data[p].data || data[p].data == "")
			cell.find('span>input').val(data[p].data);
		else
			cell.find('span>input').val(data[p]);

		if (false === data[p].visible)
			cell.style.display = 'none';
	}
}

SlaveTable.prototype.get_row = function (index)
{
	if (0 > index ||
		index >= this.egt_table.rows.length)
		return null;
	
	var ret = [];
	var row = this.egt_table.rows[index];
	$(row).find('td>span').each(function ()
	{
		ret.push({'label':$(this).children(':first').text(), 'data':$(this).children(':last').val()});
	});
	
	return ret;
}

SlaveTable.prototype.get_row_assoc = function (index)
{
	if (0 > index ||
		index >= this.egt_table.rows.length)
		return null;
	
	var ret = [];
	var row = this.egt_table.rows[index];
	$(row).find('td>span').each(function ()
	{
		ret[this.parentNode.getAttribute('_field_')] = {'label':$(this).children(':first').text(), 'data':$(this).children(':last').val()};
	});
	
	return ret;
}

SlaveTable.prototype.get_selected_row = function ()
{
	if (0 > this.egt_table.selected_index)
		return false;
	
	var ret = [];
	var row = this.egt_table.rows[this.egt_table.selected_index];
	$(row).find('td>span').each(function ()
	{
		ret.push({'label':$(this).children(':first').text(), 'data':$(this).children(':last').val()});
	});
	
	return ret;
}

SlaveTable.prototype.get_selected_row_assoc = function ()
{
	if (0 > this.egt_table.selected_index)
		return false;
	
	var ret = [];
	var row = this.egt_table.rows[this.egt_table.selected_index];
	$(row).find('td>span').each(function ()
	{
		ret[this.parentNode.getAttribute('_field_')] = {'label':$(this).children(':first').text(), 'data':$(this).children(':last').val()}
	});
	
	return ret;
}

SlaveTable.prototype.update_selected_row = function (data)
{
	if (!$.isArray(data) ||
		0 > this.egt_table.selected_index)
		return;
	
	var row = this.egt_table.rows[this.egt_table.selected_index];
	
	var cell = $(row.cells[0]);
	
	if (cell.find('input[value=_og_]').length)
		cell.find('input[value=_og_]').val('_update_');
	
	for (var i=0,j=i+1; i<data.length; i++,j++)
	{
		if (null == data[i])
			continue;
		
		cell = $(row.cells[i]);
		
		if (data[i].label)
			cell.find('span>label').text(data[i].label);
		else
			cell.find('span>label').text(data[i]);
			
		if (data[i].data)
			cell.find('span>input').val(data[i].data);
		else
			cell.find('span>input').val(data[i]);
	}
}

SlaveTable.prototype.get_colume = function (field)
{
	var ret = [];
	$(this.table).find('td[_field_=' + field + ']>span').each(function ()
	{
		ret.push({'label':$(this).children(':first').text(), 'data':$(this).children(':last').val()});
	});
	
	return ret;
}

SlaveTable.prototype.get_selected_colume = function (field)
{
	if (0 > this.egt_table.selected_index)
		return false;
	
	var span = $(this.egt_table.rows[this.egt_table.selected_index]).find('td[_field_=' + field + ']>span');
	
	return {'label':span.children(':first').text(), 'data':span.children(':last').val()};
}
/* /mothed for items */

SlaveTable.prototype.creat_contextmenu = function (options)
{
	var self = this;

	var sel = document.createElement('select');
	sel.multiple = true;
	sel.size = 7;
	sel.style.position = 'absolute';
	sel.style.display = 'none';
	sel.style.fontSize = '13px';
	sel.style.padding = '3px 5px';
	sel.style.border = '1px #ccc solid';
	
	for (var p in options.contextmenu)
	{
		var op = new Option(p, p);
		$(op).bind('click', this, options.contextmenu[p]);
		
		sel.appendChild(op);
	}
	
	document.body.appendChild(sel);
	
	if (sel.size < sel.options.length)
		sel.size = sel.options.length;
	
	var contextmunu_fn_og = document.oncontextmenu;
	var doc_click_fn_og = document.onclick;
	
	var mouse_over = function ()
	{
		if (self.disabled)
			return;
		
		document.oncontextmenu = function (evn) 
		{
			sel.style.zIndex = 99999;
			$(sel).show();
			$(sel).offset({top:evn.pageY, left:evn.pageX});
			sel.selectedIndex = -1;
			return false;
		};
		
		$(document).click(function (evn2) 
		{
			document.onclick = doc_click_fn_og;
			if (0 == evn2.button)
				$(sel).hide();
		});
	}
	
	var mouse_out = function ()
	{
		document.oncontextmenu = contextmunu_fn_og;
	}
	
	$(this.table).hover(mouse_over, mouse_out);
	
	if (this.options.contextmenu_area)
		$(this.options.contextmenu_area).hover(mouse_over, mouse_out);
	
	/*$(this.table).hover(
		function () {
			document.oncontextmenu = function (evn) {
				$(sel).show();
				$(sel).offset({top:evn.clientY, left:evn.clientX});
				sel.selectedIndex = -1;
				return false;
			};
			
			$(document).click(function (evn2) {
				document.onclick = doc_click_fn_og;
				if (0 == evn2.button)
					$(sel).hide();
			});
		},
		function () {
			document.oncontextmenu = contextmunu_fn_og;
		}
	);*/
	
	return sel;
}

SlaveTable.prototype.hide_menus = function (menus)
{
	for (var i=0; i<menus.length; i++)
	{
		$(this.contextmenu_box).find('[value=' + menus[i] + ']').css('display', 'none');
	}
}

SlaveTable.prototype.show_menu = function (menus)
{
	for (var i=0; i<menus.length; i++)
	{
		$(this.contextmenu_box).find('[value=' + menus[i] + ']').css('display', '');
	}
}