/**
 * TabEx
 *
 * @author LYQ
 */
 
var lyq = lyq || {};

lyq.TabEx = function ()
{
	
	
};

lyq.TabEx.fullsizeTab = function (tab)
{
	var top = tab.position().top;
	
	tab.css({
		'position':'absolute',
		'top':top,
		'bottom':0,
		'left':parseInt(tab.parent().css('paddingLeft')) + parseInt(tab.parent().css('marginLeft')),
		'right':parseInt(tab.parent().css('paddingRight')) + parseInt(tab.parent().css('marginRight'))
	});
	
	tab.find('.tab-content').css({
		'position':'absolute',
		'top':tab.children().first().outerHeight(true),
		'bottom':0,
		'left':0,
		'right':0
	});
}