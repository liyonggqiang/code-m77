/**
 * RelateFeilds
 *
 * @author LYQ
 */
 
var lyq = lyq || {};

lyq.RelateFields = function ()
{
	
}

lyq.RelateFields.parse = function ()
{
	$('input[rvalue]').each(function(index, element) {
        var rexp = $(this).attr('rvalue');
		if (!rexp.length)
		{
			return;
		}
		
		var matchs = rexp.match(/#\w+/ig);
		for (var p in matchs)
		{
			rexp = rexp.replace(matchs[p], '$("'+matchs[p]+'").val()');
			$(matchs[p]).change(function () {
				$(element).val(eval(rexp));
			});
		}
    });
}