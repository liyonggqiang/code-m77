/**
 * Utils
 *
 * @author LYQ
 */
 
var lyq = lyq || {};

lyq.Utils = function ()
{
	
}

lyq.Utils.bindObjectToForm = function (obj, form, map)
{
	if (map)
	{
		for (p in map)
		{
			form[map[p]] = obj[p];	
		}
		
		return;
	}
	
	for (p in obj)
	{
		form[p] = obj[p];	
	}
}