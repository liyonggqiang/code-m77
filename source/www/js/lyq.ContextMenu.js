/**
 * ContextMenu
 *
 * @author LYQ
 */
 
 /*
 
     <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
    <li><a tabindex="-1" href="#">Action</a></li>
    <li><a tabindex="-1" href="#">Another action</a></li>
    <li><a tabindex="-1" href="#">Something else here</a></li>
    <li class="divider"></li>
    <li><a tabindex="-1" href="#">Separated link</a></li>
    </ul>
 
 */
 
 var lyq = lyq || {};
 
lyq.ContextMenu = function (area, options)
{
	this.disabled = false;
	
	var self = this;
	
	var sel = $('<ul class="dropdown-menu"></ul>');
	sel.hide();
	
	for (var p in options)
	{
		var op = $('<li></li>');
		$(op).bind('click', this, options[p]);
		
		op.appendTo(sel);
		
		var a = $('<a></a>');
		a.html(p);
		a.appendTo(op);
	}
	
	sel.appendTo(document.body);
	
	var contextmunu_fn_og = document.oncontextmenu;
	var doc_click_fn_og = document.onclick;
	
	var mouse_over = function ()
	{
		if (self.disabled)
		{
			return;
		}
		
		document.oncontextmenu = function (evn) 
		{
			sel.show();
			sel.offset({top:evn.pageY, left:evn.pageX});
			return false;
		};
		
		$(document).click(function (evn2) 
		{
			document.onclick = doc_click_fn_og;
			if (0 == evn2.button)
				$(sel).hide();
		});
	}
	
	var mouse_out = function ()
	{
		if (self.disabled)
		{
			return;
		}
		
		document.oncontextmenu = contextmunu_fn_og;
	}
	
	$(area).hover(mouse_over, mouse_out);
	
	return sel;
}