/**
 * AjaxUtils
 *
 * @author LYQ
 */
 
var lyq = lyq || {};

lyq.AjaxUtlis = function ()
{
	
};

lyq.AjaxUtlis.submitAndRefresh = function (url, params)
{
	$.ajax({
		url : url,
		data : $.param(params),
		success : function(data) {
			//window.location.reload();
		}
	});
};

lyq.AjaxUtlis.submitAndRedirect = function (url, params, redirect_url)
{
	$.ajax({
		url : url,
		data : $.param(params),
		success : function(data) {
			//window.location = redirect_url;
		}
	});
};