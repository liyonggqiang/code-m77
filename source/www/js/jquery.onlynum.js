// input number
;(function($){
	$.fn.onlynum = function(settings){
		settings = $.extend( { dosome:function(){}} ,settings ||{});
		this.keydown (function (event)
		{
			if (8 == event.keyCode ||  46 == event.keyCode)
				return true;

			if ((48 > event.keyCode || (57 < event.keyCode && 96 > event.keyCode) || 105 < event.keyCode) &&
				110 != event.keyCode &&  190 != event.keyCode)
			{
				return false;
			}

			var num = this.value;
			if (110 == event.keyCode ||  190 == event.keyCode)
			{
				num += '.';
			}
			else if (96 <= event.keyCode && 105 >= event.keyCode)
			{
				num += '' + event.keyCode - 96;
			}
			else
			{
				num += String.fromCharCode(event.keyCode);
			}

			if (!/^\d\.?\d*$/.test(num))
				return false;

			 return true;
		});
		this.keyup(function(event)
		{
			var num = this.value;
			//替换所有非数字小数点
			num = num.replace(/[^\d.]/g, "");
			//第一位不能小数点
			num = num.replace(/^\./g, "");
			//两个连续的点
			num = num.replace(/\.{2,}/g, ".");
			var temp = num.split(".");
			if(temp.length >1)
			{
				num = temp.shift()+"."+temp.join("");
			}
			if(isNaN(parseFloat(num)))
			{
				num = '';
			}
			this.value = num;

			settings.dosome(this.value);
		})
	}	
})(jQuery);

(function($){
	$.fn.onlyalpha = function(settings){
		settings = $.extend( { dosome:function(){}} ,settings ||{});
		this.keydown (function (event)
		{
			if (8 == event.keyCode ||  46 == event.keyCode)
				return true;

			if (!(48 <= event.keyCode && 57 >= event.keyCode) &&
				!(96 <= event.keyCode || 105 >= event.keyCode) &&
				!(65 <= event.keyCode || 90 >= event.keyCode))
			{
				return false;
			}

			var str = this.value;
			if (96 <= event.keyCode && 105 >= event.keyCode)
			{
				str += '' + event.keyCode - 96;
			}
			else
			{
				str += String.fromCharCode(event.keyCode);
			}

			if (!/^\w+$/.test(str))
				return false;

			 return true;
		});
		this.keyup(function(event)
		{
			this.value = this.value.replace(/\W+/, '');

			settings.dosome(this.value);
		})
	}
})(jQuery);

//set readonly
;(function($){
	$.fn.setReadonly = function(settings){
		settings = $.extend( {					
		    color   : 'red',
			opacity : 1
		} , settings ||{});
		return this.filter("input[readonly='readonly']").css({'background':settings.color,'opacity':settings.opacity});
	}	
})(jQuery);

//get sum
;(function($){
	$.getSum = function(settings){
		
		settings = $.extend({ 
		    key     : 'quantity',
		    tableid : 'st',
		    dosome  : function(data){}
		} ,settings ||{});
		    
		var name = "[" + settings.key + "]";
		var tbid = "#" + settings.tableid;
		
		var getinput = $(tbid).find('input').filter("input[name$="+name+"]");
		if(!getinput.size()){
			alert("错误参数 key:"+settings.key);
			return;
		}
		var sum = 0;
		getinput.each(function(){
		    if(this.value){
		        sum =  parseInt(sum) + parseInt(this.value);
		    }
		});
		settings.dosome(sum);
	}
})(jQuery)
