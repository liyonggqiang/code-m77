/**
 * ListEx
 *
 * @author LYQ
 */
 
var lyq = lyq || {};
 
lyq.ListEx = function (box, options)
{
	this.box = $(box);
	
	var tbodies = this.box.children();
	this.head = tbodies[0];
	this.body = tbodies[1];
	this.foot = tbodies[2];
	
	this.options = options || {};
		
	$(this.head).css({
		'overflow' : 'hidden'
	});
	
	$(this.head).children(':first').css({
		'width' : '100%',
		'min-width' : '2000px'
	});
	
	$(this.body).css({
		'overflow' :  'auto'
	});
	
	if (!this.foot)
	{
		$(this.body).css({
			'bottom' :  '0px'
		});
	}
	
	$(this.body).children(':first').css({
		'width' : '100%',
		'min-width' : '2000px'
	});
	
		//add colgroup
	var head_table = $(this.head).children(':first')[0];
	var body_table = $(this.body).children(':first')[0];
	
	var colgroup = $('<colgroup></colgroup>');
	for (var i=0; i<head_table.rows[0].cells.length; i++)
	{
		var col = $('<col />');
		col.attr('width',  $(head_table.rows[0].cells[i]).outerWidth());
		colgroup.append(col);
		
		head_table.rows[0].cells[i].removeAttribute('width');
	}
	colgroup.insertBefore($(head_table).children().first());


	if (this.options.height)
	{
		$(this.body).css({'height':this.options.height});
	}
	
	if (this.options.full_size)
	{
		$(this.box).css({
			'position' : 'absolute',
			'top' : this.box.offsetTop,
			'left' : 0,
			'right' : 0,
			'bottom' : 0
		});
		
		$(this.body).css({
			'position' : 'absolute',
			'top' : $(this.head).height(),
			'left' : 0,
			'right' : 0,
			'bottom' : $(this.foot).height()
		});
		
		$(this.foot).css({
			'position' : 'absolute',
			'left' : 0,
			'right' : 0,
			'bottom' : 0
		});
		
		if (this.options.auto_size)
		{
			var win = $(window);
			var win_w_og = win.width();
			var win_w_n = 0;
			$(window).resize(function ()
			{
				win_w_n = win.width();
				
				if (win_w_n < win_w_og + 15 && win_w_n > win_w_og - 15)
					return;
				
				win_w_og = win_w_n;
				var height = 0;
				for (var node = table.previousSibling; node; node = node.previousSibling)
					height += node.offsetHeight ? node.offsetHeight : 0;
				
				$(table).css({'top' : height});		
			});
		}
		
		this.fullsize();
	}
	
	var self = this;
	$(this.body).scroll(function ()
	{
		$(self.head).scrollLeft($(self.body).scrollLeft());
	});
	
	if (options.events)
	{
		for (var e in self.options.events)
		{
			$(this.body).find('tr').bind(e, this, options.events[e]);
		}
	}
	
	if (options.slave_table)
	{
		this.slaveTable = new lyq.SlaveTable(this, options.slave_table);
	}
	
	if (options.contextmenu)
	{
		this.contextmenu = new lyq.ContextMenu(this.box, options.contextmenu);
	}
	
		//--------------------------------------------------------------------------
	var total_width = 0;
	
	colgroup.children().each(function (idx, elem) {
		total_width += parseInt(elem.getAttribute('width'));
	});
	
	var parent_width = this.box.parent().width();
	if (total_width <= parent_width)
	{
		var last_col = colgroup.children('[width!=0]').last();
		last_col.attr('width', parent_width-(total_width-last_col.attr('width')));
		
		total_width = parent_width;
	}
	
	$(this.head).children(':first').css({
		'min-width' : total_width + 'px'
	});
	
	$(this.body).children(':first').css({
		'min-width' : total_width + 'px'
	});
	
	colgroup.clone().insertBefore($(body_table).children().first());
	
		//--------------------------------------------------------------------------
	this.lastSelectedRow = null;
	$(this.body).on('click', 'tr', function () {
		if ($(this).find('input[type=checkbox]')[0].checked)
		{
			$(this).find('input[type=checkbox]')[0].checked = false;
			self.lastSelectedRow = null;
		}
		else
		{
			if (!options.mulit_selection && self.lastSelectedRow)
			{
				self.lastSelectedRow.find('input[type=checkbox]')[0].checked = false;
			}
			
			$(this).find('input[type=checkbox]')[0].checked = true;
			self.lastSelectedRow = $(this);
		}
	});
};

lyq.ListEx.prototype.fullsize = function ()
{
	var height = 0;
	
	this.box.prevAll().each(function (node, index) {
		height += $(this).outerHeight(true);
	});
	
	$(this.box).css({
		'top' : height,
		'left': parseInt($(this.box).parent().css('paddingLeft')) + parseInt($(this.box).parent().css('marginLeft')),
		'right': parseInt($(this.box).parent().css('paddingRight')) + parseInt($(this.box).parent().css('marginRight'))
	});	
};

lyq.ListEx.prototype.selectedIndex = function ()
{
	return $(this.box).find('tr:has(input:first:checked)').index();
};

lyq.ListEx.prototype.selectedIndexes = function ()
{
	return $(this.box).find('tr:has(input:first:checked)').index();
};

lyq.ListEx.prototype.dataForRow = function (index)
{
    var d = $(this.body).find('tr').eq(index).find('input[type=checkbox]').first().attr('d');
    
    try
    {
        var content = {};
        content = jQuery.parseJSON(d);
        return content;
    }
    catch (e)
    {
        return d;
    }
};

lyq.ListEx.prototype.dataForSelectedRow = function ()
{
	var d = $(this.body).find('tr').eq(this.selectedIndex()).find('input[type=checkbox]').first().attr('d');
    
    try
    {
        var content = {};
        content = jQuery.parseJSON(d);
        return content;
    }
    catch (e)
    {
        return d;
    }
}