/**
 * SlaveTable
 *
 * @author LYQ
 */
 
var lyq = lyq || {};
 
lyq.SlaveTable = function(table, options)
{
	if (!table.body)
		return;
		
	this.options = options || {};
	
	this.selectedIndex = -1;
	
	this.parent_table = $(table);
	this.head = $(table.head).find('table');
	this.table = $(table.body).find('table');
	this.disabled = false;
	
	this.post_name = options && options.post_name ? options.post_name : 'std_' + lyq.SlaveTable.instance_no;
		
	this.trash_table = $('<table>');
	this.trash_table.css('display','none');
	this.table.parent().append(this.trash_table);
		
	this.init();
	
	if (options && options.hide_fields)
	{
		this.hideFields(options.hide_fields);
	}
};

lyq.SlaveTable.prototype.currentRowCount = function ()
{
	return this.table[0].rows.length;
};

lyq.SlaveTable.prototype.init = function ()
{
	var self = this;
	
	this.fields = new Array();
	this.head.find('th').each(function () {
		self.fields.push($(this).attr('slv_field'));
	});
	
	this.table.find('tr').each(function (i)
	{
		$(this).attr('_flag_', '@');
		
		$(this).find('td').each(function (j)
		{
			var f = self.fields[j];
			
			if (!f)
			{
				$(this).attr('_field_', j);
				return;
			}
			
			$(this).attr('_field_', f);
			
			var content = {};
			
			try
			{
				content = jQuery.parseJSON($(this).text());
			}
			catch (e)
			{}
			
			if (!isNaN(content))
			{
				content = {};
			}
			
			var span = document.createElement('span');
			var label = document.createElement('label');
			label.innerHTML = content.label ? content.label : $(this).html();
			
			var input = document.createElement('input');
			input.type = 'hidden';
			input.value = content.data ? content.data : $(this).text();
			
			if (self.fields && self.fields[j])
				input.name = self.post_name + '[' + i + '][' + f + ']';
			else
				input.name = self.post_name + '[' + i + '][]';
			
			span.appendChild(label);
			span.appendChild(input);
			
			this.innerHTML = '';
			this.appendChild(span);
		});
		
			//make a row flag
		var flag_inp = document.createElement('input');
		flag_inp.type = 'hidden';
		flag_inp.name = self.post_name + '[' + i + '][_flag_]';
		flag_inp.value = '_og_';
		this.cells[0].appendChild(flag_inp);
	});
}

lyq.SlaveTable.prototype.hideFields = function (fields)
{
	fields = $(fields);
	
	var self = this;
	this.head.find('th').each(function (index, node) {
		fields.each(function(idx, elem) {
            if ($(node).attr('slv_field') == elem)
			{
				self.head.find('col').eq(index).css({'visibility':'collapse'}).attr('width','0');
				self.table.find('col').eq(index).css({'visibility':'collapse'}).attr('width','0');
			}
        });
	});
	
	
	this.head.find('th').each(function (index, node) {
		fields.each(function(idx, elem) {
            if ($(node).attr('slv_field') == elem)
			{
				$(node).css('display','none');
			}
        });
	});
	
	this.table.find('td').each(function (index, node) {
		fields.each(function(idx, elem) {
            if ($(node).attr('_field_') == elem)
			{
				$(node).css('display','none');
			}
        });
	});
}

lyq.SlaveTable.prototype.addRow = function (data)
{
	if ('object' != typeof data)
		return;

	var self = this;
	var cells = this.head[0].rows[0].cells;
	var row = this.table[0].insertRow(this.table[0].rows.length);
	row.setAttribute('_flag_', '+');
	
	var row_idx = this.currentRowCount()-1;

	for (var i=0,p=null; i<this.fields.length; i++)
	{
		var cell = row.insertCell(i);
		
		p = this.fields[i];
		var d = data[p];
		
		cell.setAttribute('width', cells[i].getAttribute('width'));
		
		if (p)
		{
			cell.setAttribute('_field_', p);
		}

		if (0 == i)
		{
			var flag_inp = document.createElement('input');
			flag_inp.type = 'hidden';
			flag_inp.name = this.post_name + '[' + row_idx + '][_flag_]';
			flag_inp.value = '+';

			cell.appendChild(flag_inp);

			if (!p)
			{
				var chkbox = document.createElement('input');
				chkbox.type = 'checkbox';
				$(chkbox).insertBefore(flag_inp);

				continue;
			}
		}
		
		if (!data[p])
			continue;

		var span = document.createElement('span');
		var label = document.createElement('label');
		if (d.label)
			label.innerHTML = d.label;
		else if(d.label == "")
			label.innerHTML = d.data;
		else
			label.innerHTML = d;

		var input = document.createElement('input');
		input.type = 'hidden';

		if (!p)
			input.name = this.post_name + '[' + row_idx + '][' + (i+1) + ']';
		else
			input.name = this.post_name + '[' + row_idx + '][' + p + ']';

		if (d.data || d.data == "")
			input.value = d.data;
		else
			input.value = d;

		span.appendChild(label);
		span.appendChild(input);

		cell.appendChild(span);
	}
}

lyq.SlaveTable.prototype.removeRow = function (index)
{
	var row = this.table[0].rows[index];
	if (!row)
	{
		return;
	}
	
	row = $(row);
	if ('+'==row.attr('_flag_'))
	{
		row.remove();
	}
	else
	{
		row.attr('_flag_', '-');
		this.trash_table.append(row);
	}
}

lyq.SlaveTable.prototype.updateRow = function (index, data)
{
	var row = this.table[0].rows[index];
	if (!row)
	{
		return;
	}
	
	row.remove();
	
	var self = this;
	var cells = this.head[0].rows[0].cells;
	row = this.table[0].insertRow(index);
	row.setAttribute('_flag_', '*');
	
	for (var i=0,p=null; i<this.fields.length; i++)
	{
		var cell = row.insertCell(i);
		
		p = this.fields[i];
		var d = data[p];
		
		cell.setAttribute('width', cells[i].getAttribute('width'));
		
		if (p)
		{
			cell.setAttribute('_field_', p);
		}

		if (0 == i)
		{
			var flag_inp = document.createElement('input');
			flag_inp.type = 'hidden';
			flag_inp.name = this.post_name + '[' + index + '][_flag_]';
			flag_inp.value = '*';

			cell.appendChild(flag_inp);

			if (!p)
			{
				var chkbox = document.createElement('input');
				chkbox.type = 'checkbox';
				cell.appendChild(chkbox);

				continue;
			}
		}
		
		//if (!data[p])
			//continue;

		var span = document.createElement('span');
		var label = document.createElement('label');
		if (d.label)
			label.innerHTML = d.label;
		else if(d.label == "")
			label.innerHTML = d.data;
		else
			label.innerHTML = d;

		var input = document.createElement('input');
		input.type = 'hidden';

		if (!p)
			input.name = this.post_name + '[' + index + '][' + (i+1) + ']';
		else
			input.name = this.post_name + '[' + index + '][' + p + ']';

		if (d.data || d.data == "")
			input.value = d.data;
		else
			input.value = d;

		span.appendChild(label);
		span.appendChild(input);

		cell.appendChild(span);
	}
}

lyq.SlaveTable.prototype.selectedRowIndex = function ()
{
	return $(this.table).find('tr:has(input:first:checked)').index();
};

lyq.SlaveTable.prototype.dataForSelectedRow = function ()
{
	var tr = $(this.table).find('tr:has(input:first:checked)');
	
	if (0 == tr.length || 1 < tr.length)
	{
		return null;
	}
	
	var ret = {};
	tr.find('td>span').each(function ()
	{
		ret[this.parentNode.getAttribute('_field_')] = {'label':$(this).children(':first').text(), 'data':$(this).children(':last').val()}
	});
	
	return ret;
}